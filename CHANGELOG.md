## 2.6.0-105

#### Features

* **Pass vaccinal:** Prise en compte du vaccin Novavax
* **Pass vaccinal:** MAJ des règles sur le vaccin Janssen

## 2.5.0-104

#### Features

* **Pass vaccinal:** Changements sur le recovery


## 2.4.2-101 (2022-01-28)

#### Features

* **Pass vaccinal:** Changements sur le pass +


## 2.4.0-99 (2022-01-12)

#### Features

* **Pass vaccinal:** Ajout du mode pass vaccinal

* **Résultats:** Prise en compte des cas rétablissement + vaccins


## 2.3.2-93 (2022-01-06)

#### Fix

* **Synchronisation:** Utilisation de Period pour les ages


## 2.3.1-92 (2022-01-03)

#### Fix

* **mode Opérateur de transport:** Correctifs


## 2.3.0-91 (2021-12-17)

#### Features

* **mode Opérateur de transport:** Gestion de nouveaux types de règle


## 2.2.2-89 (2021-12-10)

#### Features

* **Gestion 3ème dose:** Maj des règles de gestion pour la 3ème, désactivée niveau back au moment de la livraison de l'app

* **Synchronisation:** Optimisation blacklists


## 2.2.1-87 (2021-11-30)

#### Features

#### Fix

* **Synchronisation:** correctifs


## 2.1.0-83 (2021-11-24)

#### Features

* **Pass Sanitaires décommissionnés:** Nouvel affichage et nouveau message

* **modes de vérification:** • Ajustement du mode OT en fonction des nouvelles règles


## 2.0.0-78 (2021-11-09)
#### Fix

* **mode Opérateur de transport:** rajout d'un message spécifique


## 2.0.0-77

#### Features

* **Nouveau mode:** • 3 modes de vérification TAC Verif (activités), TAC Verif+ (Professionnels de Santé), OT (Opérateurs de Transport)

* **Gestion 3ème dose:** Ajout des règles de gestion pour la 3ème, désactivée niveau back au moment de la livraison de l'app

#### Fix

* **Synchronisation:** meilleur gestion de l'enchainement des appels
  
* **Résultats:**
  - Reprise des correctifs sur les librairies EU


## 1.11.1-69 (2021-10-14)

#### Fix
* **Général:** 
  - Renommage du mode OT en TAC Verif +
  - Résolution d'un mauvais affichage lors de l'activation du mode TAC Verif + 


## 1.11.0-67 (2021-10-12)

#### Features

* **Switch pour OT:** Les Opérateurs de Transport peuvent facilement basculer en Affichage Lite si un contôle ponctuel ne nécessite pas l'affichage de toutes les données

#### Fix

* **Résultats:** 
    - Les DCC Exemptions sont nommés "Pass contre-indication"
    - Il y a des contrôles en plus sur l'intégrité du DCC

* **Scan:** Le correctif pour les XCover3 est intégré à la librairie Scandit. La solution de contournement a été enlevée  


## 1.10.0-64 (2021-09-23)

#### Features

* **DCC Pass Ephémère:** Prise en compte du nouveau DCC Pass Ephémère spécifique France

#### Fix

* **Synchro:** Affichage du message de synchro obligatoire juste après une vérification


## 1.9.1-63 (2021-09-13)

#### Fix

* **Résultats:**
    - Gestion correcte de la première dose Janssen
    - Affichage des noms / prénoms par défaut si ceux récupérés sont vides (Singapour)

## 1.9.0-62 (2021-09-03)

#### Features

* **Résultats:**
  - Gestion des dates de naissance incomplètes
  - Gestion des DCC Exemptions (spécifique France)
  - Gestion des rappels de vaccins Janssen

* **Statistiques:** Suppression de l'horaire précise

* **Tutoriels:** Indicateur de la page actuelle

#### Fix

* **Scan:** Affichage correct du bouton d'aide


