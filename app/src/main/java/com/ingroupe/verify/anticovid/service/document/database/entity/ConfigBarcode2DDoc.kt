package com.ingroupe.verify.anticovid.service.document.database.entity

import com.google.gson.annotations.SerializedName

class ConfigBarcode2DDoc {
    @SerializedName("documentTypeCode")
    val documentTypeCode: String? = null

    @SerializedName("resourceType")
    val resourceType: String? = null
}