package com.ingroupe.verify.anticovid.service.document.dcc.preparation

import com.ingroupe.verify.anticovid.service.document.dcc.DccModeInterface
import dgca.verifier.app.decoder.model.Exemption
import java.time.LocalDate

class ExemptionPreparation(
    val dateFrom: LocalDate,
    val dateUntil: LocalDate
) {
    companion object {

        fun getPreparation(
            exemption: Exemption
        ) : ExemptionPreparation? {
            val dateFrom = DccModeInterface.getLocalDateFromDcc(exemption.dateValidFrom)
            val dateUntil = DccModeInterface.getLocalDateFromDcc(exemption.dateValidUntil)

            return if(dateFrom == null || dateUntil == null) {
                null
            } else {
                ExemptionPreparation(dateFrom, dateUntil)
            }
        }

    }
}
