/*
 *  ---license-start
 *  eu-digital-green-certificates / dgca-verifier-app-android
 *  ---
 *  Copyright (C) 2021 T-Systems International GmbH and all other contributors
 *  ---
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  ---license-end
 *
 *  Created by osarapulov on 6/25/21 9:21 AM
 */

package com.ingroupe.verify.anticovid.service.api.configuration.engine

import com.fasterxml.jackson.databind.ObjectMapper
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.DescriptionResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.RuleResult
import dgca.verifier.app.engine.data.Description
import dgca.verifier.app.engine.data.Rule
import dgca.verifier.app.engine.data.RuleCertificateType
import dgca.verifier.app.engine.data.Type
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*


fun RuleResult.toRule(): Rule = Rule(


    identifier = this.identifier,
    type = Type.valueOf(this.type.uppercase(Locale.ROOT)),
    version = this.version,
    schemaVersion = this.schemaVersion,
    engine = this.engine,
    engineVersion = this.engineVersion,
    ruleCertificateType = RuleCertificateType.valueOf(this.certificateType.uppercase(Locale.ROOT)),
    descriptions = this.descriptions.toDescriptions(),
    validFrom = ZonedDateTime.parse(this.validFrom, DateTimeFormatter.ISO_ZONED_DATE_TIME),
    validTo = ZonedDateTime.parse(this.validTo, DateTimeFormatter.ISO_ZONED_DATE_TIME),
    affectedString = this.affectedString,
    logic = ObjectMapper().readTree(this.logic.toString()),
    countryCode = this.countryCode.lowercase(Locale.ROOT),
    region = this.region,
    modifier = this.modifier
)

fun List<RuleResult>.toRules(): List<Rule> {
    val rules = mutableListOf<Rule>()
    for (i in this.indices) {
        val ruleRemote = this[i]
        rules.add(ruleRemote.toRule())
    }
    return rules
}

fun DescriptionResult.toDescriptions(): Description = Description(
    lang = this.lang,
    desc = this.desc
)

fun List<DescriptionResult>.toDescriptions(): Map<String, String> {
    val descriptions = mutableMapOf<String, String>()
    for (i in this.indices) {
        val descriptionRemote = this[i]
        descriptions[descriptionRemote.lang.lowercase(Locale.ROOT)] = descriptionRemote.desc
    }
    return descriptions
}