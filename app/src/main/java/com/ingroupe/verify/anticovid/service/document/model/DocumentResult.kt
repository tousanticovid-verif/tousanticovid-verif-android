package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DocumentResult(
    @SerializedName("data")
    var data: DocumentDataResult? = null,
    @SerializedName("errors")
    var errors: ArrayList<DocumentErrorsResult>? = null,
    @SerializedName("resourceType")
    var resourceType: String? = null,
    @SerializedName("engineData")
    val engineData: DocumentEngineDataResult? = null

) : Serializable