package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DocumentEngineResult(

    @SerializedName("zoneLabel")
    var zoneLabel: String? = null,
    @SerializedName("listRules")
    var listRules: List<DocumentRuleResult>? = null,
    @SerializedName("checkColorCountries")
    var checkColorCountries: Boolean = false,
    @SerializedName("noIcon")
    var noIcon: Boolean = false,
    @SerializedName("infoMessage")
    var infoMessage: String? = null


    ) : Serializable {

    fun isCheckedByEngine(): Boolean {
        return listRules?.isNotEmpty() == true
    }

    fun isValidatedByEngine(): Boolean {
        return listRules?.filter { !it.isValid }.isNullOrEmpty()
    }
}