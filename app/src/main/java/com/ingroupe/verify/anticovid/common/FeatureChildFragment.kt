package com.ingroupe.verify.anticovid.common

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.ingroupe.verify.anticovid.MainActivity

abstract class FeatureChildFragment : Fragment() {
    var featureFragment: FeatureFragment? = null

    abstract fun getTitle(): String?

    abstract fun getTitleId(): Int?

    open fun canPressBack(): Boolean = true

    abstract fun showNavigation(): MainActivity.NavigationIcon?

    open fun getOrientation(): Int = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity?.invalidateOptionsMenu()
        activity?.requestedOrientation = getOrientation()

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    open fun getSoftInputMode() = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    open fun isScanBlocked() = false

}