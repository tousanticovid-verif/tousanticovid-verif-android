package com.ingroupe.verify.anticovid.service.api.configuration.blacklist

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BlacklistElement(
    @SerializedName("id")
    var id: Int,
    @SerializedName("hash")
    var hash: String,
    @SerializedName("active")
    var active: Boolean? = true
): Serializable
