package com.ingroupe.verify.anticovid.ui.scan

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration
import com.ingroupe.verify.anticovid.service.document.DocOfflineService
import kotlinx.coroutines.*

class ScanPresenterImpl(
    private val view: ScanView
) : ScanPresenter {

    companion object {
        const val TAG = "Scan2DDocP"
    }

    override fun on2dDocDetected(context: Context, result2DDoc: String, travelConfiguration: TravelConfiguration?): Boolean {

        val documentResult = DocOfflineService.getDocumentBy2DDoc(result2DDoc ,context, travelConfiguration) ?: return false
        view.showResult(documentResult)
        return true
    }

    override fun checkExpirationAndDo(model: SharedViewModel, context: Context, lmbd: () -> Unit) {

        val isExpired = JWTUtils.loadConf(model, true)

        if(isExpired) {
            val dialog = AlertDialog.Builder(context)
                .setCancelable(false)
                .setTitle(context.getString(R.string.popup_licence_expired_title))
                .setMessage(context.getString(R.string.popup_licence_expired_text))
                .setPositiveButton(context.getString(R.string.action_ok)) { dialog, _ ->
                    dialog.dismiss()
                    (context as MainActivity).changeMode(true)
                    lmbd()
                }
                .create()
            dialog.show()
        } else {
            lmbd()
        }
    }
    override fun checkDcc(qrCode: String, context: Context, travelConfiguration: TravelConfiguration?) {
        loadDocumentResult(qrCode, context, Constants.DccFormat.DCC_EU, travelConfiguration)

    }

    override fun checkDccExemption(qrCode: String, context: Context, travelConfiguration: TravelConfiguration?) {
        loadDocumentResult(qrCode, context, Constants.DccFormat.DCC_EXEMPTION, travelConfiguration)
    }

    override fun checkDccActivity(qrCode: String, context: Context, travelConfiguration: TravelConfiguration?) {
        loadDocumentResult(qrCode, context, Constants.DccFormat.DCC_ACTIVITY, travelConfiguration)
    }

    private fun loadDocumentResult(
        qrCode: String,
        context: Context,
        format: Constants.DccFormat,
        travelConfiguration: TravelConfiguration?
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            supervisorScope {
                try {
                    val dcc = async {
                        DocOfflineService.getDocumentByDcc(
                            qrCode,
                            context,
                            format,
                            travelConfiguration
                        )
                    }
                    val loadedDcc = dcc.await()

                    withContext(Dispatchers.Main) {
                        view.onDccLoaded(loadedDcc)
                    }
                } catch (e: Exception) {
                    view.onDccLoaded(null)
                }
            }
        }
    }
}