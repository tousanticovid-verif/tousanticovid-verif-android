package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncExclusionTime(
    @SerializedName("start")
    var start: String,
    @SerializedName("end")
    var end: String
)