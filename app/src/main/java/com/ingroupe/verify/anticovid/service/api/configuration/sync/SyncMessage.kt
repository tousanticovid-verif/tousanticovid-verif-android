package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncMessage (
    @SerializedName("titleFR")
    var titleFR: String? = null,
    @SerializedName("detailFR")
    var detailFR: String? = null,
    @SerializedName("titleEN")
    var titleEN: String? = null,
    @SerializedName("detailEN")
    var detailEN: String? = null
)