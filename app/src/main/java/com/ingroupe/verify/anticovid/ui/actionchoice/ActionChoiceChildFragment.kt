package com.ingroupe.verify.anticovid.ui.actionchoice

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioGroup
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.FeatureChildFragment
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.databinding.ActionChoicesMainBinding
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import com.ingroupe.verify.anticovid.synchronization.elements.Labels
import com.ingroupe.verify.anticovid.ui.actionchoice.configuration.ConfPagerAdapter
import com.ingroupe.verify.anticovid.ui.scan.ScanChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialscan2ddoc.TutorialScanActivity
import java.time.LocalDate


class ActionChoiceChildFragment : FeatureChildFragment(), ActionChoiceView {
    override fun getTitle(): String = "Vérification"
    override fun getTitleId(): Int = R.string.title_action_choice

    private var presenter: ActionChoicePresenter? = null

    private lateinit var model: SharedViewModel

    companion object {
        const val TAG = "actionChoice"
        fun newInstance() = ActionChoiceChildFragment()
    }
    private var _binding: ActionChoicesMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = ActionChoicesMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")


        if (presenter == null) {
            presenter = ActionChoicePresenterImpl()
        }

        binding.clScan2ddoc.setOnClickListener {
            goToScan2DDoc()
        }
    }

    override fun onResume() {
        super.onResume()

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        manageDisplay()
    }

    private fun manageDisplay() {
        binding.clOtExpiration.visibility = View.GONE
        togglePremiumConfPanel(isVisible = false)
        context?.let { c ->
            initLiteView(c)
            updateDisplayMode(c, JWTUtils.getDisplayMode())
            if (JWTUtils.isPremium()) {
                togglePremiumConfPanel(isVisible = true)
                initConfigView()

                binding.layoutContentOtConfig.textViewOtMessage.visibility = View.GONE
                SynchronisationUtils.getRuleEngineMessageOT()?.let { message ->
                    binding.layoutContentOtConfig.textViewOtMessage.visibility = View.VISIBLE
                    binding.layoutContentOtConfig.textViewOtMessage.text = message
                }

                val nbDaysBeforeExpiration = JWTUtils.daysBeforeExpiration()
                if (nbDaysBeforeExpiration == 0) {
                    val nbHoursBeforeExpiration = JWTUtils.hoursBeforeExpiration()
                    binding.textViewOtExpText1.text = getString(
                        R.string.action_ot_expiration_text1_hours,
                        nbHoursBeforeExpiration
                    )
                    binding.clOtExpiration.visibility = View.VISIBLE

                } else if (nbDaysBeforeExpiration <= 20) {
                    binding.textViewOtExpText1.text = getString(
                        R.string.action_ot_expiration_text1,
                        nbDaysBeforeExpiration
                    )
                    binding.clOtExpiration.visibility = View.VISIBLE
                }
                initModeSwitcher(c)
            } else {
                togglePremiumConfPanel(isVisible = false)
                (c as MainActivity).changeMode(true)
            }

            if (SynchronisationUtils.getCurrentStep() == 3) {
                binding.clScan2ddoc.isEnabled = false
                binding.imageView2ddoc.setImageResource(R.drawable.ic_icon_2ddoc_black)
                binding.textViewScan2ddoc.text = getString(R.string.scan_synchronization_alert)
                togglePremiumConfPanel(isVisible = false)
                binding.layoutContentLiteConfig.configMainLayout.visibility = View.GONE
            } else {
                SynchronisationUtils.checkStep(c)
            }
        }
    }

    private fun togglePremiumConfPanel(isVisible: Boolean) {
        val visibility = if (isVisible) View.VISIBLE else View.GONE
        binding.tabLayoutMode.visibility = visibility
        binding.layoutContentOtConfig.configMainLayout.visibility = visibility
    }

    private fun initModeSwitcher(context: Context) {
        if (binding.tabLayoutMode.tabCount == 0) {
            val listTabs = listOf(
                    getString(R.string.action_switch_display_lite),
                    getString(R.string.action_switch_display_premium),
                    getString(R.string.action_switch_display_ot))

            listTabs.map {
                binding.tabLayoutMode.addTab(
                    binding.tabLayoutMode.newTab().setText(it)
                )
            }
            binding.tabLayoutMode.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    handleTabSelected(tab, context)
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {}
                override fun onTabReselected(tab: TabLayout.Tab?) {
                    handleTabSelected(tab, context)
                }
            })
        }

        when (JWTUtils.getDisplayMode()) {
            Constants.DisplayMode.VACCINE -> {
                binding.tabLayoutMode.getTabAt(0)?.select()
            }
            Constants.DisplayMode.HEALTH -> {
                binding.tabLayoutMode.getTabAt(0)?.select()
            }
            Constants.DisplayMode.PS -> {
                binding.tabLayoutMode.getTabAt(1)?.select()
            }
            Constants.DisplayMode.OT -> {
                binding.tabLayoutMode.getTabAt(2)?.select()
            }
        }
    }

    private fun handleTabSelected(tab: TabLayout.Tab?, context: Context) {
        when (tab?.position) {
            0 -> {
                // Le pass vaccinal n'est pas activé tout de suite
                if(SynchronisationUtils.isPassVaccineStarted()) {
                    if(JWTUtils.getDisplayMode() == Constants.DisplayMode.HEALTH) {
                        updateDisplayMode(context, Constants.DisplayMode.HEALTH)
                    } else {
                        updateDisplayMode(context, Constants.DisplayMode.VACCINE)
                    }
                } else {
                    updateDisplayMode(context, Constants.DisplayMode.HEALTH)
                }
            }
            1 -> updateDisplayMode(context, Constants.DisplayMode.PS)
            2 -> updateDisplayMode(context, Constants.DisplayMode.OT)
        }
    }

    private fun updateDisplayMode(context: Context, displayMode: Constants.DisplayMode) {
        when (displayMode) {
            Constants.DisplayMode.VACCINE -> {
                binding.textViewCurrentMode.text = getString(R.string.action_scan_lite_vaccine_mode)
                binding.layoutContentOtConfig.configMainLayout.visibility = View.GONE
                // Le pass vaccinal n'est pas activable tout de suite
                if(SynchronisationUtils.isPassVaccineStarted()) {
                    binding.layoutContentLiteConfig.configMainLayout.visibility = View.VISIBLE
                    binding.layoutContentLiteConfig.switchPassLite.isChecked = true
                    binding.layoutContentLiteConfig.textViewActionSwitch.text =
                        getString(R.string.action_switch_to_health_pass)
                    binding.layoutContentLiteConfig.textViewActionSwitch.setTextAppearance(R.style.basic_text)
                    binding.layoutContentLiteConfig.tvModeDetails.text =
                        Labels.getLabel(
                            Constants.LabelsEnum.DESCRIPTION,
                            Constants.ControlMode.VACCINE
                        )
                    binding.layoutContentLiteConfig.tvCurrentMode.text =
                        getString(R.string.action_switch_display_title_vaccine)
                } else {
                    binding.layoutContentLiteConfig.configMainLayout.visibility = View.GONE
                }
            }
            Constants.DisplayMode.HEALTH -> {
                binding.textViewCurrentMode.text = getString(R.string.action_scan_lite_health_mode)
                binding.layoutContentOtConfig.configMainLayout.visibility = View.GONE
                // Le pass vaccinal n'est pas activable tout de suite
                if(SynchronisationUtils.isPassVaccineStarted()) {
                    binding.layoutContentLiteConfig.configMainLayout.visibility = View.VISIBLE
                    binding.layoutContentLiteConfig.switchPassLite.isChecked = false
                    binding.layoutContentLiteConfig.textViewActionSwitch.text =
                        getString(R.string.action_switch_to_vaccine_pass)
                    binding.layoutContentLiteConfig.textViewActionSwitch.setTextAppearance(R.style.basic_title_14)
                    binding.layoutContentLiteConfig.tvModeDetails.text =
                        Labels.getLabel(
                            Constants.LabelsEnum.DESCRIPTION,
                            Constants.ControlMode.HEALTH
                        )
                    binding.layoutContentLiteConfig.tvCurrentMode.text =
                        getString(R.string.action_switch_display_title_health)
                } else {
                    binding.layoutContentLiteConfig.configMainLayout.visibility = View.GONE
                }
            }
            Constants.DisplayMode.PS -> {
                binding.textViewCurrentMode.text = getString(R.string.action_scan_premium_mode)
                binding.layoutContentOtConfig.configMainLayout.visibility = View.GONE
                binding.layoutContentLiteConfig.configMainLayout.visibility = View.GONE
            }
            Constants.DisplayMode.OT -> {
                binding.textViewCurrentMode.text = getString(R.string.action_scan_ot_mode)
                binding.layoutContentOtConfig.configMainLayout.visibility = View.VISIBLE
                binding.layoutContentLiteConfig.configMainLayout.visibility = View.GONE
            }
        }

        JWTUtils.changeDisplayMode(displayMode, model)
        (context as MainActivity).changeMode(!displayMode.isPremium)
    }

    private fun initConfigView() {
        val pagerAdapter = ConfPagerAdapter(this)
        binding.layoutContentOtConfig.pagerConfig.adapter = pagerAdapter
        binding.layoutContentOtConfig.pagerConfig.isUserInputEnabled = false

        TabLayoutMediator(binding.layoutContentOtConfig.tabLayout, binding.layoutContentOtConfig.pagerConfig, true, false) { tab, position ->
            when (position) {
                0 -> tab.text = getString(R.string.conf_departure)
                else -> tab.text = getString(R.string.conf_arrival)
            }
        }.attach()

        val pageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> binding.layoutContentOtConfig.imageviewConfType.setImageResource(R.drawable.ic_takeoff)
                    else -> binding.layoutContentOtConfig.imageviewConfType.setImageResource(R.drawable.ic_landing)
                }
            }
        }
        binding.layoutContentOtConfig.pagerConfig.registerOnPageChangeCallback(pageChangeCallback)

        if (presenter?.getSavedTravelType() == Constants.TravelType.ARRIVAL) {
            binding.layoutContentOtConfig.pagerConfig.setCurrentItem(1, false)
        }
    }

    private fun initLiteView(context: Context) {
        binding.layoutContentLiteConfig.switchPassLite.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener,
            CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
            }

            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if(isChecked) {
                    updateDisplayMode(context, Constants.DisplayMode.VACCINE)
                } else {
                    updateDisplayMode(context, Constants.DisplayMode.HEALTH)
                }
            }
        })
    }

    private fun goToScan2DDoc() {
        val sharedPref = activity?.getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
        val showTuto = sharedPref?.getBoolean(Constants.SavedItems.SHOW_SCAN_TUTO.text, true)
        featureFragment?.replaceFragment(ScanChildFragment.TAG)

        if (showTuto == true) {
            val i = Intent(activity, TutorialScanActivity::class.java)
            i.putExtra(Constants.CAN_SKIP_TUTO, true)
            activity?.startActivity(i)
        }
    }
    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.SHOW_SLIDE
    }

    override fun isScanBlocked() : Boolean {
        return !binding.clScan2ddoc.isEnabled
    }
}