package com.ingroupe.verify.anticovid

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.navigation.NavigationView
import com.ingroupe.verify.anticovid.common.*
import com.ingroupe.verify.anticovid.databinding.ActivityMainBinding
import com.ingroupe.verify.anticovid.dialog.LoadingAnimDialog
import com.ingroupe.verify.anticovid.observer.*
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import com.ingroupe.verify.anticovid.synchronization.elements.Config
import com.ingroupe.verify.anticovid.ui.actionchoice.ActionChoiceChildFragment
import com.ingroupe.verify.anticovid.ui.init.InitChildFragment
import com.ingroupe.verify.anticovid.ui.result.ResultScanChildFragment
import com.ingroupe.verify.anticovid.ui.scan.ScanChildFragment
import com.ingroupe.verify.anticovid.ui.statistic.StatisticChildFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    BackgroundObserverListener, ConfServiceUtils.ConfServiceListener, SynchronisationUtils.SyncListener {

    companion object {
        const val TAG = "MainActivity"
        private const val NO_FRAGMENT_TAG = "NO_FRAGMENT"
        private const val CURRENT_TAG_KEY = "CURRENT_TAG"
    }
    private lateinit var binding: ActivityMainBinding

    private var currentTag: String? = NO_FRAGMENT_TAG
    private var currentFeatureFragment: FeatureFragment? = null

    private var toggleSave: DrawerLayout.DrawerListener? = null
    private val showProgressBarHandler = Handler(Looper.getMainLooper())

    private val localeBr = LocaleChangedBroadcastReceiver()

    private var snackBarIdentifier: String? = null

    enum class NavigationIcon {
        SHOW_SLIDE, BACK, NO_ICON, NO_MENU
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)

        val toggle = ActionBarDrawerToggle(
            this, binding.drawerLayout, binding.appBarMain.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.navView.setNavigationItemSelectedListener(this)

        toggleSave = toggle

        currentTag = savedInstanceState?.getSerializable(CURRENT_TAG_KEY) as? String

        localeBr.setMainActivityHandler(this)
        val filter = IntentFilter(Intent.ACTION_LOCALE_CHANGED)
        registerReceiver(localeBr, filter)

        if (NO_FRAGMENT_TAG == currentTag) {
            replaceFragment(InitChildFragment.TAG)
        } else {
            replaceFragment(currentTag ?: InitChildFragment.TAG)
        }

        ProcessLifecycleOwner.get().lifecycle.addObserver(BackgroundObserver(this))

        transferSettings()

        SynchronisationUtils.startWorker()
        if(BuildConfig.LOG_ENABLED) {
            binding.imageViewLogoIN.setOnTouchListener { v, event ->
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        FileLogger.send(this@MainActivity)
                        FileLogger.clean(this@MainActivity)
                    }
                }
                v?.onTouchEvent(event) ?: true
            }
        }
    }

    override fun onResume() {
        Log.d(TAG, "on resume")
        super.onResume()

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }

        //Checks the latest sync state
        checkSyncState()

    }

    override fun onDestroy() {
        Log.d(TAG, "on destroy")
        super.onDestroy()
        unregisterReceiver(localeBr)
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            when {
                currentFeatureFragment?.getCurrentTag() == ScanChildFragment.TAG -> {
                    currentFeatureFragment?.popToTag(ActionChoiceChildFragment.TAG)
                }
                currentFeatureFragment?.getCurrentTag() == ResultScanChildFragment.TAG -> {
                    currentFeatureFragment?.popToTag(ScanChildFragment.TAG)
                }
                currentFeatureFragment?.onBackPressed() == false -> {
                    super.onBackPressed()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        binding.appBarMain.toolbar.setNavigationOnClickListener(null)
        supportActionBar?.show()
        //navigation
        currentFeatureFragment?.showNavigation()?.let { navigationIcon ->
            when (navigationIcon) {

                NavigationIcon.SHOW_SLIDE -> {
                    Log.d("nav", "menu")

                    supportActionBar?.setDisplayHomeAsUpEnabled(false)
                    toggleSave?.let {
                        binding.drawerLayout.addDrawerListener(it)
                        (it as ActionBarDrawerToggle).syncState()
                        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED)
                        binding.appBarMain.toolbar.setNavigationIcon(R.drawable.ic_baseline_menu_24)
                    }

                    binding.appBarMain.toolbar.setNavigationOnClickListener {
                        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                            binding.drawerLayout.closeDrawer(GravityCompat.START)
                        } else {
                            binding.drawerLayout.openDrawer(GravityCompat.START)
                        }
                    }
                }
                NavigationIcon.BACK -> {
                    Log.d("nav", "back")
                    supportActionBar?.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material)
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    supportActionBar?.setDisplayShowHomeEnabled(true)

                    binding.appBarMain.toolbar.setNavigationOnClickListener { onBackPressed() }

                    binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                }
                NavigationIcon.NO_ICON -> {
                    Log.d("nav", "none")
                    supportActionBar?.setHomeAsUpIndicator(android.R.color.transparent)
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)

                    toggleSave?.let {
                        binding.drawerLayout.removeDrawerListener(it)
                    }
                    //toolbar.setNavigationIcon(null)
                    binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                }
                NavigationIcon.NO_MENU -> {
                    supportActionBar?.hide()
                }
            }
        }
        return true
    }

    //Called from the init Fragment once the initial setup is finished
    fun onEngineInitDone() {
        NetworkLiveData.init(this.application)
        NetworkLiveData.observe(this, {
            if(it && SynchronisationUtils.isSyncNeeded()) {
                ConfServiceUtils.callSynchronization(this, TAG, true)
            }
        })
    }

    fun reloadConfiguration() {

        val model = run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        }
        model.reloadConfiguration = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return currentFeatureFragment?.onOptionsItemSelected(item) ?: true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                Log.d("nav", "home")
                currentFeatureFragment?.replaceFragment(ActionChoiceChildFragment.TAG)
            }
            R.id.nav_settings -> {
                Log.d("nav", "settings")
                currentFeatureFragment?.replaceFragment(SettingsChildFragment.TAG)
            }
            R.id.nav_info -> {
                Log.d("nav", "information")
                currentFeatureFragment?.replaceFragment(InformationChildFragment.TAG)
            }
            R.id.nav_help -> {
                Log.d("nav", "help")

                val dialog = AlertDialog.Builder(ContextThemeWrapper(this,
                    R.style.AlertDialogCustom
                ))
                    .setTitle(getString(R.string.help_title))
                    .setView(R.layout.help_view)
                    .setPositiveButton(getString(R.string.action_ok), null)
                    .create()

                dialog.show()


                val model = run {
                    ViewModelProvider(this).get(SharedViewModel::class.java)
                }
                model.configuration?.confAbout.let {
                    dialog.findViewById<TextView>(R.id.textView_help_url)?.text =
                        getString(R.string.help_part3_conf, it?.url)
                }

                var version = ""
                try {
                    val pInfo = packageManager.getPackageInfo(packageName, 0)
                    version = pInfo.versionName
                } catch (e: PackageManager.NameNotFoundException) {
                    e.printStackTrace()
                }

                dialog.findViewById<TextView>(R.id.textView_Prefix)?.text = getString(R.string.help_version, version)
            }
            R.id.nav_stat -> {
                Log.d("nav", "statistic")
                currentFeatureFragment?.replaceFragment(StatisticChildFragment.TAG)
            }
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }


    private fun replaceFragment(tag: String): Boolean {
        Log.d(TAG, "replace fragment")
        var fragmentCreated = false
        var fragment = supportFragmentManager.findFragmentByTag(tag)
        val transaction = supportFragmentManager.beginTransaction()
        val fragments = supportFragmentManager.fragments
        for (aFragment in fragments) {
            transaction.hide(aFragment)
        }

        if (fragment == null) {
            fragmentCreated = true
            fragment = when (tag) {
                NavigationFragment.TAG -> NavigationFragment.newInstance()
                else -> NavigationFragment.newInstance()
            }
            transaction.add(R.id.main_container, fragment, tag).commit()
        } else {
            transaction.show(fragment).commit()
        }

        if (fragment is FeatureFragment) {
            currentFeatureFragment = fragment
        }

        currentFeatureFragment?.getTitle()?.let { title = it }

        currentTag = tag
        return fragmentCreated
    }

    fun hideKeyboard() {
        val view = findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    private fun showProgressBar(show: Boolean) {
        Log.d(TAG, "show progress bar $show")
        showProgressBarHandler.post {
            Log.d(TAG, "is running")
            if (show && LoadingAnimDialog.getInstance()?.isAdded == false) {
                LoadingAnimDialog.getInstance()?.show(supportFragmentManager, TAG)
                Log.d(TAG, "loadingAnimDialog?.show()")
            } else if (!show && LoadingAnimDialog.getInstance()?.isAdded == true) {
                LoadingAnimDialog.getInstance()?.dismissAllowingStateLoss()
                Log.d(TAG, "loadingAnimDialog?.dismiss()")
            }
        }
    }

    @Suppress("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onShowLoadingAnimEvent(showLoadingAnimEvent: ShowLoadingAnimEvent) {
        Log.d(TAG, "show loading anim event ${showLoadingAnimEvent.show}")
        this.runOnUiThread {
            if (showLoadingAnimEvent.activity == this) {
                showProgressBar(showLoadingAnimEvent.show)
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun checkSyncState() {
        Config.checkIfAppUpdated()
        if(Config.isNewAppVersionAvailable()) {
            if(Config.isNewAppVersionMajor()) {
                showUpdateAppDialog(this, isDismissable = false)
            } else {
                showMinorAppVersion(this)
            }
        } else {
            this.hideSnackBar(Constants.SNACKBAR_APP_VERSION)
        }
    }

    private fun showMinorAppVersion(context: Context) {
        if(context is MainActivity) {
            context.showSnackBar(
                Constants.SNACKBAR_APP_VERSION,
                context.getString(R.string.snackbar_version_needed_line_1),
                context.getString(R.string.snackbar_version_needed_line_2),
                criticity = Constants.Criticity.CRITICAL
            ) {
                showUpdateAppDialog(context, true)
            }
        }
    }

    private fun showUpdateAppDialog(context: Context, isDismissable: Boolean) {
        val dialogBuilder = AlertDialog.Builder(
            ContextThemeWrapper(
                context,
                R.style.AlertDialogCustom
            )
        )
            .setTitle(getString(R.string.popup_update_title))
            .setView(R.layout.popup_update_app)
            .setCancelable(isDismissable)
            .setPositiveButton(getString(R.string.action_update)) { dialog, _ ->
                openPlayStore()
                if (isDismissable) {
                    dialog.dismiss()
                }
            }

        if (isDismissable) {
            dialogBuilder.setNegativeButton(getString(R.string.action_cancel)) { dialog, _ -> dialog.dismiss() }
        }

        dialogBuilder.create()
        dialogBuilder.show()
    }

    private fun openPlayStore() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.STORE_MARKET_URI)))
        } catch (e: ActivityNotFoundException)  {
            val intent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(Constants.STORE_URI )
                setPackage(Constants.STORE_PACKAGE)
            }
            startActivity(intent)
        }
    }

    override fun hasComeBackFromBackground() {
        currentFeatureFragment?.forceToActionChoice()
        if(SynchronisationUtils.isSyncNeeded()) {
            ConfServiceUtils.callSynchronization(this, TAG, true)
        }
    }

    fun changeMode(isModeLite: Boolean) {
        if(isModeLite) {
            binding.appBarMain.appBarLayout.setBackgroundColor(getColor(R.color.in_white))
            binding.appBarMain.appBarLayout.context.setTheme(R.style.AppTheme_AppBarOverlay)
            binding.appBarMain.toolbar.setTitleTextColor(getColor(R.color.in_blue))
            binding.appBarMain.appBarLayout.invalidate()

            var drawable = binding.appBarMain.toolbar.navigationIcon
            if(drawable != null)  {
                drawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(drawable.mutate(), getColor(R.color.in_blue))
                binding.appBarMain.toolbar.navigationIcon = drawable
            }
        } else {
            binding.appBarMain.appBarLayout.setBackgroundColor(getColor(R.color.in_blue))
            binding.appBarMain.appBarLayout.context.setTheme(R.style.AppTheme_AppBarOverlayOT)
            binding.appBarMain.toolbar.setTitleTextColor(getColor(R.color.in_white))
            binding.appBarMain.appBarLayout.invalidate()

            var drawable = binding.appBarMain.toolbar.navigationIcon
            if(drawable != null)  {
                drawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(drawable.mutate(), getColor(R.color.in_white))
                binding.appBarMain.toolbar.navigationIcon = drawable
            }
        }

    }

    fun showSnackBar(identifier: String, messageLine1: String, messageLine2: String?, criticity: Constants.Criticity = Constants.Criticity.INFO, listener: View.OnClickListener?) {
        snackBarIdentifier = identifier

        binding.appBarMain.contentMain.textViewCsLine1.text = messageLine1

        if(messageLine2 == null) {
            binding.appBarMain.contentMain.textViewCsLine2.visibility = View.GONE
        } else {
            binding.appBarMain.contentMain.textViewCsLine2.visibility = View.VISIBLE
            binding.appBarMain.contentMain.textViewCsLine2.text = messageLine2
        }

        val color = when(criticity) {
            Constants.Criticity.INFO -> ResourcesCompat.getColor(resources, R.color.field_orange, null)
            Constants.Criticity.CRITICAL -> ResourcesCompat.getColor(resources, R.color.field_red, null)
        }
        binding.appBarMain.contentMain.clCustomSnackbar.setBackgroundColor(color)

        binding.appBarMain.contentMain.clCustomSnackbar.visibility = View.VISIBLE
        binding.appBarMain.contentMain.clCustomSnackbar.setOnClickListener(listener)
    }

    fun hideSnackBar(identifier: String) {
        if(snackBarIdentifier == identifier) {
            binding.appBarMain.contentMain.clCustomSnackbar.visibility = View.GONE
            binding.appBarMain.contentMain.clCustomSnackbar.setOnClickListener(null)
        }
    }

    override fun showErrorMessage(title: Int, message: Int) {
        Log.i(TAG, "Error during sync (network available)")
    }

    override fun saveResult(syncResult: SyncResult?) {
        SynchronisationUtils.saveSynchronization(
            syncResult,
            this,
            this,
            this,
            TAG,
            true
        )
    }

    override fun showLoading(show: Boolean) {
        // nothing to do
    }

    override fun onSynchronizationDone() {
        if (currentFeatureFragment?.getCurrentTag() == ActionChoiceChildFragment.TAG && currentFeatureFragment?.isScanBlocked() == true) {
            currentFeatureFragment?.refreshFragment()
        }
    }

    override fun onSynchronizationError() {
        // nothing to do
    }

    private fun transferSettings() {

        val sharedPref = getPreferences(Context.MODE_PRIVATE)

        val settingsPref =
            App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)

        with(settingsPref.edit()) {

            sharedPref.getBoolean(Constants.SavedItems.SHOW_SCAN_TUTO.text, true).let {
                if(!it) {
                    putBoolean(Constants.SavedItems.SHOW_SCAN_TUTO.text, it)
                }
            }
            sharedPref.getBoolean(Constants.SavedItems.SHOW_RESULT_TUTO.text, true).let {
                if(!it) {
                    putBoolean(Constants.SavedItems.SHOW_RESULT_TUTO.text, it)
                }
            }
            sharedPref.getBoolean(Constants.SavedItems.INFO_CGU_POLICY_SHOWN_V2.text, false).let {
                if(it) {
                    putBoolean(Constants.SavedItems.INFO_CGU_POLICY_SHOWN_V2.text, it)
                }
            }
            sharedPref.getLong(Constants.SavedItems.CONF_DATE_EXP.text, -1L).let {
                if(it != -1L) {
                    putLong(Constants.SavedItems.CONF_DATE_EXP.text, it)
                }
            }
            sharedPref.getString(Constants.SavedItems.CURRENT_TOKEN.text, null)?.let {
                putString(Constants.SavedItems.CURRENT_TOKEN.text, it)
            }
            sharedPref.getString(Constants.SavedItems.CURRENT_SIREN.text, null)?.let {
                putString(Constants.SavedItems.CURRENT_SIREN.text, it)
            }
            sharedPref.getString(Constants.SavedItems.DISPLAY_MODE.text, null)?.let {
                putString(Constants.SavedItems.DISPLAY_MODE.text, it)
            }
            apply()
        }

        with(sharedPref.edit()) {
            clear()
            apply()
        }
    }
}
