package com.ingroupe.verify.anticovid.ui.init

import android.content.Context
import android.util.Log
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.service.barcode.database.BarcodeLoader
import com.ingroupe.verify.anticovid.service.document.database.DocumentLoader
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class InitPresenterImpl(
    private val view: InitView
) : InitPresenter, CoroutineScope {

    companion object {
        const val TAG = "InitP"
    }
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    override fun loadAssets(model: SharedViewModel) {
        BarcodeLoader.loadAssets()
        DocumentLoader.loadAssets()

        JWTUtils.loadConf(model, false)
    }

    override fun initEngineIfNeeded() {
        val sharedPrefs = App.getContext().getSharedPreferences(Constants.SYNC_KEY, Context.MODE_PRIVATE)
        val isInit = sharedPrefs.getBoolean(Constants.KEY_LAST_FORCED_UPDATE, false)
        val dateInit = sharedPrefs.getLong(Constants.KEY_DATE_UPDATE_CONF, 0)

        if (!isInit || SynchronisationUtils.getDateLastSynchronization().time < dateInit) {
            Log.d(TAG, "Launch Init")
            launch {
                supervisorScope {
                    try {
                        val setupAsync = async { SynchronisationUtils.initializeEngineDatas() }
                        val setupComplete : Boolean = withTimeout(12000) { setupAsync.await() }
                        if (setupComplete) {
                            with(sharedPrefs.edit()) {
                                putBoolean(
                                    Constants.KEY_LAST_FORCED_UPDATE,
                                    true
                                )
                                apply()
                            }
                        }
                    } catch (exception: Exception) {
                        Log.e(TAG,
                            "Error while loading the init of the DBs : ${exception.stackTraceToString()}"
                        )
                    } finally {
                        withContext(Dispatchers.Main) {
                            view.setupFinished()
                        }
                    }
                }
            }
        } else {
            view.setupFinished()
        }
    }

    override fun tearDown() {
        job.cancel()
    }

}