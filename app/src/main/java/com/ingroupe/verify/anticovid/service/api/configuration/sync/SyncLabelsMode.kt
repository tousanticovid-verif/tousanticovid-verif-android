package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncLabelsMode  (
    @SerializedName("description")
    var description: SyncLabel? = null,
    @SerializedName("tutorial")
    var tutorial: SyncLabel? = null
)