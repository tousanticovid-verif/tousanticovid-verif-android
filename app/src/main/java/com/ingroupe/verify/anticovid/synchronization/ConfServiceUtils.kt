package com.ingroupe.verify.anticovid.synchronization

import android.util.Log
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.auth.WSConf
import com.ingroupe.verify.anticovid.common.FileLogger
import com.ingroupe.verify.anticovid.common.toSublistsOf
import com.ingroupe.verify.anticovid.service.api.configuration.blacklist.BlacklistResult
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.RuleIdentifierResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.RuleResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.valuesets.ValueSetIdentifierResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.valuesets.ValueSetResult
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import com.ingroupe.verify.anticovid.synchronization.elements.Analytics
import com.ingroupe.verify.anticovid.synchronization.elements.Blacklist
import com.ingroupe.verify.anticovid.synchronization.elements.Config
import com.scandit.datacapture.core.internal.sdk.AppAndroidEnvironment.applicationContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*

abstract class ConfServiceUtils {

    companion object {

        private const val TAG = "ConfServiceUtils"

        private const val URI_SYNC = "\\synchronisation\\tacv"
        private const val URI_ANALYTICS = "\\synchronisation"
        private const val URI_CONF = "\\configuration\\tacv"

        private const val URI_RULE_IDENTIFIERS = "\\rules\\tacv"
        private const val URI_RULES = "\\rules\\hashes\\tacv"

        private const val URI_VALUESET_IDENTIFIERS = "\\valuesets\\tacv"
        private const val URI_VALUESETS = "\\valuesets\\hashes\\tacv"

        private const val URI_COUNTRIES = "\\countries\\tacv"

        private const val RULES_PER_QUERY = 200
        private const val URI_BLACKLIST = "\\blacklist\\tacv\\"

        fun callSynchronization(
            view: ConfServiceListener,
            tag: String,
            backgroundWork: Boolean
        ) {
            val text =  if (backgroundWork)  " start from worker" else  " start from user"
            FileLogger.write(Date().toString() + text, applicationContext)
            Log.d(tag, "callSynchronization")
            view.showLoading(true)

            val configurationService = WSConf.getConfigurationService()

            configurationService.callSync(WSConf.getConfigurationBaseUrl() + URI_SYNC).enqueue(
                object : Callback<SyncResult> {
                    override fun onFailure(call: Call<SyncResult>, t: Throwable) {
                        Log.e(tag, "callSync failure")
                        view.showLoading(false)
                        if (!backgroundWork) {
                            when {
                                call.isCanceled -> view.showErrorMessage(
                                    R.string.error_call_no_connection_title,
                                    R.string.error_call_no_network_message
                                )
                                else -> view.showErrorMessage(
                                    R.string.error_call_no_connection_title,
                                    R.string.error_call_failure
                                )
                            }
                        }
                    }

                    override fun onResponse(
                        call: Call<SyncResult>,
                        response: Response<SyncResult>
                    ) {
                        Log.d(tag, "callSync onResponse")

                        when (response.code()) {
                            200 -> {
                                //Not dismissing the loader because it will be dismissed later
                                Log.d(tag, "OK :")
                                val syncResult = response.body()
                                view.saveResult(syncResult)
                            }
                            else -> {
                                view.showLoading(false)
                                Log.e(tag, "KO :" + response.code())
                                if (!backgroundWork) {
                                    view.showErrorMessage(
                                        R.string.error_call_error_title,
                                        R.string.error_call_http_ko
                                    )
                                }
                            }
                        }
                    }
                }
            )
        }


        fun sendAnalytics(
            logs: List<String>,
            listener: ConfServiceListener,
            tag : String,
            backgroundWork: Boolean
        ) {
            val configurationService = WSConf.getConfigurationService()
            configurationService.sendAnalytics(WSConf.getConfigurationBaseUrl() + URI_ANALYTICS, logs).enqueue(
                object : Callback<Unit> {
                    override fun onFailure(call: Call<Unit>, t: Throwable) {
                        Log.e(tag, "sendLogs onFailure", t)
                        FileLogger.write("sendAnalytics fail " + t.message, applicationContext)

                        if(!backgroundWork) {
                            when {
                                call.isCanceled -> listener.showErrorMessage(
                                    R.string.error_call_no_connection_title,
                                    R.string.error_call_no_network_message
                                )
                                else -> listener.showErrorMessage(
                                    R.string.error_call_no_connection_title,
                                    R.string.error_call_failure
                                )
                            }
                        }
                    }

                    override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                        Log.d(tag, "sendLogs onResponse")
                        when (response.code()) {
                            200 -> {
                                Log.d(tag, "OK")
                                Analytics.deleteOldStats()
                            }
                            else -> {
                                Log.e(tag, "KO :" + response.code())
                                if(!backgroundWork) {
                                    listener.showErrorMessage(
                                        R.string.error_call_error_title,
                                        R.string.error_call_http_ko
                                    )
                                }
                            }
                        }
                    }
                }
            )
        }

        fun callConfiguration(
            listener: ConfServiceListener,
            tag : String,
            backgroundWork: Boolean
        ) {

            val configurationService = WSConf.getConfigurationService()
            configurationService.callConfiguration(WSConf.getConfigurationBaseUrl() + URI_CONF).enqueue(
                object : Callback<ConfResult> {
                    override fun onFailure(call: Call<ConfResult>, t: Throwable) {
                        Log.e(tag, "callConfiguration onFailure", t)
                        FileLogger.write("callConfiguration fail " + t.message, applicationContext)
                        if(!backgroundWork) {
                            when {
                                call.isCanceled -> listener.showErrorMessage(
                                    R.string.error_call_no_connection_title,
                                    R.string.error_call_no_network_message
                                )
                                else -> listener.showErrorMessage(
                                    R.string.error_call_no_connection_title,
                                    R.string.error_call_failure
                                )
                            }
                        }
                    }

                    override fun onResponse(call: Call<ConfResult>, response: Response<ConfResult>) {
                        Log.d(tag, "callConfiguration onResponse")
                        when (response.code()) {
                            200 -> {
                                Log.d(tag, "OK")
                                val confResult = response.body()
                                Config.updateConfig(confResult?.confAbout)
                            }
                            else -> {
                                Log.e(tag, "KO :" + response.code())
                                if(!backgroundWork) {
                                    listener.showErrorMessage(
                                        R.string.error_call_error_title,
                                        R.string.error_call_http_ko
                                    )
                                }
                            }
                        }
                    }
                }
            )
        }


        fun getRulesIdentifiers(): List<RuleIdentifierResult> {
            val configurationService = WSConf.getConfigurationService()
            val response = configurationService.getRulesIdentifiers(WSConf.getConfigurationBaseUrl() + URI_RULE_IDENTIFIERS)
                .execute()
            val body = response.body()
            FileLogger.write("received " + body?.size + " rulesId", applicationContext )
            return body ?: listOf()
        }

        fun getRules(ruleHashes: List<String>): List<RuleResult> {
            val configurationService = WSConf.getConfigurationService()

            val sublistsHashes = ruleHashes.toSublistsOf(RULES_PER_QUERY)
            val listResult = ArrayList<RuleResult>()

            for (hashes in sublistsHashes) {
                val result = configurationService.getRules(WSConf.getConfigurationBaseUrl() + URI_RULES, hashes).execute().body() ?: listOf()
                FileLogger.write("received " + result.size + " rules", applicationContext )
                listResult.addAll(result)
            }

            return listResult
        }

        fun getValueSetIdentifiers(): List<ValueSetIdentifierResult> {
            val configurationService = WSConf.getConfigurationService()
            val response = configurationService.getValueSetsIdentifiers(WSConf.getConfigurationBaseUrl() + URI_VALUESET_IDENTIFIERS).execute()
            val body = response.body()
            FileLogger.write("received " + body?.size + " valuesSetIdentifiers", applicationContext )
            return  body ?: listOf()
        }

        fun getValueSet(listHash: List<String>): List<ValueSetResult> {
            val configurationService = WSConf.getConfigurationService()
            val response = configurationService.getValueSets(WSConf.getConfigurationBaseUrl() + URI_VALUESETS, listHash).execute()
            val body = response.body()
            FileLogger.write("received " + body?.size + " valuesSet", applicationContext )
            return body ?: listOf()
        }

        fun getCountries(): List<String> {
            val configurationService = WSConf.getConfigurationService()
            val response = configurationService.getCountries(WSConf.getConfigurationBaseUrl() + URI_COUNTRIES).execute()
            val body = response.body()
            FileLogger.write("received " + body?.size + " countries", applicationContext )
            return body ?: listOf()
        }

        fun getBlacklistPage(blacklistType: Blacklist.BlacklistType, firstId: Int): BlacklistResult {
            val configurationService = WSConf.getConfigurationService()
            Log.d(TAG, "Start getBlacklistPage")
            return try {
                configurationService.getBlacklist(WSConf.getConfigurationBaseUrl() + URI_BLACKLIST + blacklistType.typeForUrl + "\\" + firstId).execute().body() ?: BlacklistResult()
            } catch (e: Exception) {
                Log.d(TAG, "Error while fetching the BL ${e.message}")
                BlacklistResult()
            }
        }
    }

    interface ConfServiceListener {
        fun showErrorMessage(title: Int, message: Int)
        fun saveResult(syncResult: SyncResult?)
        fun showLoading(show: Boolean)
    }
}
