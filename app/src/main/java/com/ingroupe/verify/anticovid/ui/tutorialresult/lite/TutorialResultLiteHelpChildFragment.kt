package com.ingroupe.verify.anticovid.ui.tutorialresult.lite

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.adapter.IAdapterInterfaceClick
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.FeatureChildFragment
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.databinding.TutorialResultLiteMainBinding
import com.ingroupe.verify.anticovid.synchronization.elements.Labels

class TutorialResultLiteHelpChildFragment : FeatureChildFragment(), TutorialResultLiteView, IAdapterInterfaceClick {
    override fun getTitle(): String = "Résultat - Tutoriel"
    override fun getTitleId(): Int = R.string.title_tutorial_result

    companion object {
        const val TAG = "tutorialResultHelp"
        fun newInstance() = TutorialResultLiteHelpChildFragment()
    }
    private var _binding: TutorialResultLiteMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var model: SharedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = TutorialResultLiteMainBinding.inflate(inflater, container, false)
        val view = binding.root

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        binding.webViewTuto.loadData(
            Labels.getLabel(Constants.LabelsEnum.TUTORIAL, JWTUtils.getDisplayMode().controlMode),
            "text/html", "base64")
        binding.webViewTuto.setBackgroundColor(Color.TRANSPARENT)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonTutoToResult.visibility = View.GONE
        binding.checkBoxShowTutoResult.visibility = View.GONE
    }


    override fun onResume() {
        Log.d(TAG, "on Resume")
        super.onResume()
    }

    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.BACK
    }

    override fun onItemClick(position: Int) {
        // rien à faire
    }
}