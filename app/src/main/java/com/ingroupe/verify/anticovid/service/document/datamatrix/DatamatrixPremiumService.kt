package com.ingroupe.verify.anticovid.service.document.datamatrix

import android.content.Context
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.document.datamatrix.preparation.TestPreparation
import com.ingroupe.verify.anticovid.service.document.datamatrix.preparation.VaccinationPreparation
import com.ingroupe.verify.anticovid.service.document.model.DocumentStatic2ddocResult
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.*

object DatamatrixPremiumService : DatamatrixModeInterface {

    override fun get2ddocTest(
        static2ddoc: DocumentStatic2ddocResult,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal: String = Constants.GlobalValidity.KO.text
        var validityGlobalStatus: String = context.getString(R.string.result_invalid)

        if (!static2ddoc.hasValidSignature) {
            validityGlobal = Constants.GlobalValidity.KO.text
            validityGlobalStatus = context.getString(R.string.invalid_2ddoc_signature)
        } else {
            TestPreparation.getPreparation(static2ddoc, mappedDynamicData)?.let { prep ->

                if (!prep.isPCR && !prep.isAntigenic) {
                    validityGlobal = Constants.GlobalValidity.KO.text
                    validityGlobalStatus = context.getString(R.string.result_test_type_not_valid)
                } else {

                    when {
                        prep.valueF5 == "X" -> {
                            validityGlobal = Constants.GlobalValidity.KO.text
                            validityGlobalStatus = context.getString(R.string.dynamic_B2F5_X)
                        }
                        prep.valueF5 == "I" -> {
                            validityGlobal = Constants.GlobalValidity.KO.text
                            validityGlobalStatus = context.getString(R.string.dynamic_B2F5_I)
                        }
                        prep.isAntigenic -> {
                            if(prep.valueF5 == TestPreparation.NEGATIVE_TEST) {
                                validityGlobal = Constants.GlobalValidity.INFO.text
                                validityGlobalStatus = context.getString(R.string.result_test_antigenic_negative_info)
                            } else {
                                validityGlobal = Constants.GlobalValidity.INFO.text
                                validityGlobalStatus = context.getString(R.string.result_test_antigenic_positive_info)
                            }
                        }
                        else -> {
                            validityGlobal = Constants.GlobalValidity.INFO.text
                            validityGlobalStatus = if(prep.valueF5 == TestPreparation.NEGATIVE_TEST) {
                                context.getString(R.string.result_test_pcr_negative_info)
                            } else {
                                context.getString(R.string.result_test_pcr_positive_info)
                            }
                        }
                    }
                }

                val date =
                    SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).parse(prep.valueF6)

                date?.let { dateF6 ->
                    val dateParis = ZonedDateTime.ofInstant(dateF6.toInstant(), ZoneId.of("Europe/Paris"))
                    var minutes = ChronoUnit.MINUTES.between(dateParis, controlTimeOT?:ZonedDateTime.now())

                    val hours = minutes /60
                    minutes %= 60

                    mappedDynamicData["samplingDuration"] =
                        if(hours <= 96) {
                            context.getString(R.string.result_duration_hours_minutes, hours, minutes)
                        } else {
                            context.getString(R.string.result_duration_days_hours, hours/24, hours%24)
                        }
                }
            }
        }

        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    override fun get2ddocVaccin(
        static2ddoc: DocumentStatic2ddocResult,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal: String = Constants.GlobalValidity.KO.text
        var validityGlobalStatus: String = context.getString(R.string.result_invalid)

        if (!static2ddoc.hasValidSignature) {
            validityGlobal = Constants.GlobalValidity.KO.text
            validityGlobalStatus = context.getString(R.string.invalid_2ddoc_signature)
        } else {
            VaccinationPreparation.getPreparation(static2ddoc, mappedDynamicData)?.let { prep ->

                if (prep.valueLA == VaccinationPreparation.VACCINE_CYCLE_TE) {
                    validityGlobal = Constants.GlobalValidity.INFO.text
                    validityGlobalStatus = context.getString(R.string.result_vaccine_cycle_completed_info)
                } else {
                    validityGlobal = Constants.GlobalValidity.KO.text
                    validityGlobalStatus = context.getString(R.string.result_vaccine_cycle_improper)
                }

                val currentDate = LocalDate.now()

                val period = Period.between(prep.vacDate, controlTimeOT?.toLocalDate()?: currentDate)
                val months = period.months + period.years * 12
                mappedDynamicData["vaccinationDuration"] =
                    if(months > 0) {
                        context.getString(R.string.result_duration_months_days, months, period.days)
                    } else {
                        context.getString(R.string.result_duration_days, period.days)
                    }
            }
        }

        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }
}