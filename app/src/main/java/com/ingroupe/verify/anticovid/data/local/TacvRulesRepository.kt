package com.ingroupe.verify.anticovid.data.local

import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.data.local.rules.TacvRulesLocalDataSource
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.RuleIdentifierResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.rules.RuleResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.toRuleIdentifiers
import com.ingroupe.verify.anticovid.service.api.configuration.engine.toRules
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import dgca.verifier.app.engine.data.Rule
import dgca.verifier.app.engine.data.RuleCertificateType
import dgca.verifier.app.engine.data.RuleIdentifier
import dgca.verifier.app.engine.data.Type
import dgca.verifier.app.engine.data.source.rules.RulesRepository
import java.time.ZonedDateTime

class TacvRulesRepository(
    private val localDataSource: TacvRulesLocalDataSource
) : RulesRepository {

    companion object {
        const val TAG = "TacvRulesRepository"
    }

    override suspend fun loadRules(rulesUrl: String) {

        val listRulesIdentifierResult = ConfServiceUtils.getRulesIdentifiers()

        if(listRulesIdentifierResult.isNotEmpty()) {

            val upToDateRuleIdentifiers =
                listRulesIdentifierResult.toRuleIdentifiers().toMutableSet()

            val rulesToBeRemovedIdentifiers = localDataSource.getRuleIdentifiers()
                .filter { !upToDateRuleIdentifiers.remove(it) }
                .map { it }

            localDataSource.removeRules(rulesToBeRemovedIdentifiers)

            val hashesToGet = mutableListOf<String>()
            upToDateRuleIdentifiers.forEach { ruleIdentifier ->
                hashesToGet.add(ruleIdentifier.hash)
            }

            if(hashesToGet.isNotEmpty()) {
                val listRuleResult = ConfServiceUtils.getRules(hashesToGet)
                filterAndSaveRules(listRuleResult, upToDateRuleIdentifiers)
            }
        } else {
            throw Exception("Rules Identifiers is Empty")
        }
    }

    private fun filterAndSaveRules(
        listRuleResult: List<RuleResult>,
        upToDateRuleIdentifiers: MutableSet<RuleIdentifier>
    ) : Boolean {
        val ruleIdentifiersToBeSaved = mutableListOf<RuleIdentifier>()
        val rulesToBeSaved = mutableListOf<Rule>()

        listRuleResult.toRules().forEach { r ->
            upToDateRuleIdentifiers.find { ri -> ri.identifier == r.identifier && ri.version == r.version }?.let { newRi ->
                ruleIdentifiersToBeSaved.add(newRi)
                rulesToBeSaved.add(r)
            }
        }

        if (ruleIdentifiersToBeSaved.isNotEmpty()) {
            localDataSource.addRules(ruleIdentifiersToBeSaved, rulesToBeSaved)
            return true
        }

        return false
    }

    override fun initDatas() : Boolean {
        val rulesIdentifierResult : List<RuleIdentifierResult>? = Utils.loadFromAsset("sync/sync_rules_id.json")
        val rulesResult : List<RuleResult>? = Utils.loadFromAsset("sync/sync_rules.json")

        if (rulesResult.isNullOrEmpty() || rulesIdentifierResult.isNullOrEmpty()) {
            return false
        }

        localDataSource.removeAllRules()
        return filterAndSaveRules(rulesResult, rulesIdentifierResult.toRuleIdentifiers().toMutableSet())
    }

    override fun getRulesBy(
        countryIsoCode: String,
        validationClock: ZonedDateTime,
        type: Type,
        ruleCertificateType: RuleCertificateType
    ): List<Rule> =
        localDataSource.getRulesBy(countryIsoCode, validationClock, type, ruleCertificateType)


    fun getRulesBy(
        departureCountryCode: String,
        arrivalCountryCode: String,
        validationClock: ZonedDateTime,
        type: Type,
        ruleCertificateType: RuleCertificateType
    ): List<Rule> =
        localDataSource.getRulesBy(departureCountryCode, arrivalCountryCode, validationClock, type, ruleCertificateType)

    fun getDistinctDepartureCountry(): Set<String> = localDataSource.getDistinctDepartureCountry()
}