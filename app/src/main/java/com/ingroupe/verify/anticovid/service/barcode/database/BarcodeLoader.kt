package com.ingroupe.verify.anticovid.service.barcode.database

import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.service.barcode.database.entity.DocumentType
import com.ingroupe.verify.anticovid.service.barcode.database.entity.DocumentTypeField
import com.ingroupe.verify.anticovid.service.barcode.database.entity.Header
import com.ingroupe.verify.anticovid.service.barcode.database.entity.HeaderField

object BarcodeLoader {

    private var initialized = false

    private var headers: List<Header>? = null
    private var headerFields: List<HeaderField>? = null
    private var documentTypes: List<DocumentType>? = null
    private var documentTypeFields: List<DocumentTypeField>? = null


    fun loadAssets() {
        if(!initialized) {
            headers = Utils.loadFromAsset("barcode/headers.json")
            headerFields = Utils.loadFromAsset("barcode/headers-fields.json")
            documentTypes = Utils.loadFromAsset("barcode/document-types.json")
            documentTypeFields = Utils.loadFromAsset("barcode/document-types-fields.json")
            initialized = true
        }
    }

    fun findHeaderFieldByName(name: String): HeaderField? {
        return headerFields?.find { it.name == name }
    }

    fun findDocumentTypeFieldByName(name: String): DocumentTypeField? {
        return documentTypeFields?.find { it.name == name }
    }

    fun findDocumentTYpeByType(type: String): DocumentType? {
        return documentTypes?.find { it.type == type }
    }

    fun findHeaderByVersion(version: String): Header? {
        return headers?.find { it.version == version }
    }

}