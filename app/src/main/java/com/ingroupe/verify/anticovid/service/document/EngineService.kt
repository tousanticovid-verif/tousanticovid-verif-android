package com.ingroupe.verify.anticovid.service.document

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.CountryUtils
import com.ingroupe.verify.anticovid.common.model.Country
import com.ingroupe.verify.anticovid.common.model.RulesFavorite
import com.ingroupe.verify.anticovid.common.model.RulesForVerification
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration
import com.ingroupe.verify.anticovid.data.EngineManager
import com.ingroupe.verify.anticovid.service.document.model.DocumentEngineResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentRuleResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentStaticDccResult
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import dgca.verifier.app.decoder.cbor.GreenCertificateData
import dgca.verifier.app.decoder.getJsonDataFromAsset
import dgca.verifier.app.decoder.model.GreenCertificate
import dgca.verifier.app.engine.*
import dgca.verifier.app.engine.data.CertificateType
import dgca.verifier.app.engine.data.ExternalParameter
import dgca.verifier.app.engine.data.Rule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.time.ZonedDateTime

class EngineService {

    companion object {
        const val TAG = "EngineService"

        const val CODE_FRENCH_TERRITORIES = "_F"
        const val CODE_ORANGE_TERRITORIES = "_O"
        const val CODE_RED_TERRITORIES = "_R"
        const val CODE_OTHER_TERRITORY = "Autre"
    }

    private val engineMap: MutableMap<String, CertLogicEngine> = mutableMapOf()

    private fun getEngine(context: Context, jsonFileName: String): CertLogicEngine {
        if(!engineMap.containsKey(jsonFileName)) {
            val jsonSchema = getJsonDataFromAsset(context, jsonFileName) ?: ""
            val engine: CertLogicEngine = DefaultCertLogicEngine(
                DefaultAffectedFieldsDataRetriever(ObjectMapper().readTree(jsonSchema), ObjectMapper()),
                DefaultJsonLogicValidator())
            engineMap[jsonFileName] = engine
        }
        return engineMap[jsonFileName]!!
    }

    private fun validateRules (
        greenCertificateData: GreenCertificateData,
        countryName: String,
        arrivalCountryCode: String,
        base64EncodedKid: String,
        context: Context,
        dccFormat: Constants.DccFormat,
        controlTime: ZonedDateTime,
        rules: List<Rule>,
        validityGlobalStatus: String?
    ): DocumentEngineResult {
        greenCertificateData.apply {
            val engineCertificateType = this.greenCertificate.getEngineCertificateType()

            val issuingCountry =
                if(this.issuingCountry?.isNotBlank() == true) this.issuingCountry!! else this.greenCertificate.getIssuingCountry().lowercase()

            val valueSetsMap = mutableMapOf<String, List<String>>()
            runBlocking(Dispatchers.IO) {
                EngineManager.valueSetsRepository.getValueSets().forEach { valueSet ->
                    val ids = mutableListOf<String>()
                    valueSet.valueSetValues.fieldNames().forEach { id ->
                        ids.add(id)
                    }
                    valueSetsMap[valueSet.valueSetId] = ids
                }
            }

            val externalParameter = ExternalParameter(
                validationClock = controlTime,
                valueSets = valueSetsMap,
                countryCode = arrivalCountryCode,
                exp = this.expirationTime,
                iat = this.issuedAt,
                issuerCountryCode = issuingCountry,
                kid = base64EncodedKid,
                region = "",
            )

            val validationResults = getEngine(context, dccFormat.jsonSchemaAssetFileName).validate(
                engineCertificateType,
                this.greenCertificate.schemaVersion,
                rules,
                externalParameter,
                this.hcertJson
            )

            val listRules = mutableListOf<DocumentRuleResult>()
            validationResults.forEach {
                val ruleResult = DocumentRuleResult(
                    it.rule,
                    false
                )
                if(it.rule.modifier == Constants.ModifierType.EXCLUSION.text) {
                    return DocumentEngineResult(
                        countryName,
                        null,
                        checkColorCountries = false,
                        noIcon = false,
                        validityGlobalStatus
                    )
                }
                if(it.result == Result.PASSED) {
                    ruleResult.isValid = true
                }
                listRules.add(ruleResult)
            }

            return DocumentEngineResult(
                countryName,
                listRules
            )
        }
    }

    private fun GreenCertificate.getEngineCertificateType(): CertificateType {
        return when {
            this.recoveryStatements?.isNotEmpty() == true -> CertificateType.RECOVERY
            this.vaccinations?.isNotEmpty() == true -> CertificateType.VACCINATION
            this.tests?.isNotEmpty() == true -> CertificateType.TEST
            this.exemption != null -> CertificateType.EXEMPTION
            else -> CertificateType.TEST
        }
    }

    fun prepareRules(
        gcd: GreenCertificateData,
        travelConfiguration: TravelConfiguration
    ) : RulesForVerification {
        val countries = CountryUtils.getCountries()
        val rulesForVerification = RulesForVerification()

        val issuingCountry =
            if(gcd.issuingCountry?.isNotBlank() == true) gcd.issuingCountry!! else gcd.greenCertificate.getIssuingCountry().lowercase()

        when (travelConfiguration.travelType) {
            Constants.TravelType.ARRIVAL -> {

                val arrivalCountry = countries.find { it.nameCode == travelConfiguration.controlZone }

                travelConfiguration.favoritesList?.forEach { departureCountryCode ->

                    val departureCountry = countries.find { it.nameCode == departureCountryCode }

                    runBlocking(Dispatchers.Default) {

                        if(departureCountry?.nameCode?.isNotBlank() == true
                            && arrivalCountry?.nameCode?.isNotBlank() == true) {

                            EngineService().getRulesArrival(
                                travelConfiguration.timeUTC!!,
                                departureCountry,
                                arrivalCountry,
                                issuingCountry,
                                gcd.greenCertificate.getEngineCertificateType(),
                                rulesForVerification
                            )
                        }
                    }
                }
            }
            Constants.TravelType.DEPARTURE -> {

                val departureCountry = countries.find { it.nameCode == travelConfiguration.controlZone }

                travelConfiguration.favoritesList?.forEach { arrivalCountryCode ->

                    val arrivalCountry = countries.find { it.nameCode == arrivalCountryCode }

                    runBlocking(Dispatchers.Default) {

                        if(departureCountry?.nameCode?.isNotBlank() == true
                            && arrivalCountry?.nameCode?.isNotBlank() == true) {

                            EngineService().getRulesDeparture(
                                travelConfiguration.timeUTC!!,
                                departureCountry,
                                arrivalCountry,
                                issuingCountry,
                                gcd.greenCertificate.getEngineCertificateType(),
                                rulesForVerification
                            )
                        }
                    }
                }
            }
        }
        return rulesForVerification
    }

    fun executeRules(
        listEngineResults : MutableList<DocumentEngineResult>,
        rulesForVerification : RulesForVerification,
        staticDcc: DocumentStaticDccResult,
        dccFormat: Constants.DccFormat,
        controlTime: ZonedDateTime,
        validityGlobalStatus: String?,
        context: Context
    ) {
        runBlocking(Dispatchers.Default) {
            rulesForVerification.listRulesFavorite.map { rf ->
                async {
                    when {
                        rf.checkColorRules -> {
                            DocumentEngineResult(
                                rf.country.name,
                                null,
                                true
                            )
                        }
                        rf.showGenericMessage -> {
                            DocumentEngineResult(
                                rf.country.name,
                                null,
                                checkColorCountries = false,
                                noIcon = false,
                                validityGlobalStatus
                            )
                        }
                        else -> {
                            EngineService().validateRules(
                                staticDcc.dccData!!,
                                rf.country.name,
                                rf.arrivalCountryCode,
                                staticDcc.base64EncodedKid!!,
                                context,
                                dccFormat,
                                controlTime,
                                rf.rules,
                                validityGlobalStatus
                            )
                        }
                    }
                }
            }.map {
                listEngineResults.add(it.await())
            }

            if(rulesForVerification.greenRules.isNotEmpty()
                || rulesForVerification.orangeRules.isNotEmpty()
                || rulesForVerification.redRules.isNotEmpty()) {

                listEngineResults.add(
                    DocumentEngineResult(
                        context.getString(R.string.result_color_countries),
                        null,
                        checkColorCountries = false,
                        noIcon = true
                    )
                )

                if (rulesForVerification.greenRules.isNotEmpty()) {
                    listEngineResults.add(
                        EngineService().validateRules(
                            staticDcc.dccData!!,
                            context.getString(R.string.result_country_green),
                            rulesForVerification.arrivalCountryCode,
                            staticDcc.base64EncodedKid!!,
                            context,
                            dccFormat,
                            controlTime,
                            rulesForVerification.greenRules,
                            validityGlobalStatus
                        )
                    )
                } else {
                    listEngineResults.add(
                        DocumentEngineResult(
                            context.getString(R.string.result_country_green),
                            null,
                            checkColorCountries = false,
                            noIcon = false,
                            validityGlobalStatus
                        )
                    )
                }


                if (rulesForVerification.orangeRules.isNotEmpty()) {
                    listEngineResults.add(
                        EngineService().validateRules(
                            staticDcc.dccData!!,
                            context.getString(R.string.result_country_orange),
                            rulesForVerification.arrivalCountryCode,
                            staticDcc.base64EncodedKid!!,
                            context,
                            dccFormat,
                            controlTime,
                            rulesForVerification.orangeRules,
                            validityGlobalStatus
                        )
                    )
                } else {
                    listEngineResults.add(
                        DocumentEngineResult(
                            context.getString(R.string.result_country_orange),
                            null,
                            checkColorCountries = false,
                            noIcon = false,
                            validityGlobalStatus
                        )
                    )
                }

                if (rulesForVerification.redRules.isNotEmpty()) {
                    listEngineResults.add(
                        EngineService().validateRules(
                            staticDcc.dccData!!,
                            context.getString(R.string.result_country_red),
                            rulesForVerification.arrivalCountryCode,
                            staticDcc.base64EncodedKid!!,
                            context,
                            dccFormat,
                            controlTime,
                            rulesForVerification.redRules,
                            validityGlobalStatus
                        )
                    )
                } else {
                    listEngineResults.add(
                        DocumentEngineResult(
                            context.getString(R.string.result_country_red),
                            null,
                            checkColorCountries = false,
                            noIcon = false,
                            validityGlobalStatus
                        )
                    )
                }

            }
        }
    }

    private fun getRulesArrival(
        controlTime: ZonedDateTime,
        departureCountry: Country,
        arrivalCountry: Country,
        issuingCountry: String,
        engineCertificateType: CertificateType,
        rulesForVerification: RulesForVerification
    ) {
        val rulesUseCase = EngineManager.rulesUseCase
        var rules = listOf<Rule>()

        //SI pays de départ <> "International"
        if(departureCountry.nameCode != "Inter") {

            //SI présence d’une règle spécifique [Pays de départ]/[Pays de destination] ALORS utilisation de cette règle
            rules = rulesUseCase.invoke(
                controlTime,
                departureCountry.nameCode,
                arrivalCountry.nameCode,
                issuingCountry,
                engineCertificateType
            )

            // SINON SI le pays de départ fait partie d’un territoire Français ET SI il existe une règle correspondante [Territoire France]/[Pays de départ]
            if (rules.isEmpty() && departureCountry.isNational) {
                rules = rulesUseCase.invoke(
                    controlTime,
                    CODE_FRENCH_TERRITORIES,
                    arrivalCountry.nameCode,
                    issuingCountry,
                    engineCertificateType
                )
            }
        }

        // SINON SI présence d’une règle en fonction de la couleur du pays de départ
        //	ALORS Utilisation des 3 règles (3 réponses)
        //      OU utilisation d’une ou deux règles + Réponse bleue hors moteur

        if(rules.isEmpty()) {

            if(rulesForVerification.greenRules.isEmpty()) {
                rulesForVerification.greenRules = rulesUseCase.invoke(
                    controlTime,
                    "",
                    arrivalCountry.nameCode,
                    issuingCountry,
                    engineCertificateType
                )
            }

            if(rulesForVerification.orangeRules.isEmpty()) {
                rulesForVerification.orangeRules = rulesUseCase.invoke(
                    controlTime,
                    CODE_ORANGE_TERRITORIES,
                    arrivalCountry.nameCode,
                    issuingCountry,
                    engineCertificateType
                )
            }

            if(rulesForVerification.redRules.isEmpty()) {
                rulesForVerification.redRules = rulesUseCase.invoke(
                    controlTime,
                    CODE_RED_TERRITORIES,
                    arrivalCountry.nameCode,
                    issuingCountry,
                    engineCertificateType
                )
            }

            rulesForVerification.arrivalCountryCode = arrivalCountry.nameCode

            if(rulesForVerification.greenRules.isEmpty()
                && rulesForVerification.orangeRules.isEmpty()
                && rulesForVerification.redRules.isEmpty()) {
                rulesForVerification.listRulesFavorite.add(
                    RulesFavorite(
                        departureCountry,
                        listOf(),
                        arrivalCountry.nameCode,
                        true
                    )
                )
            } else {
                rulesForVerification.listRulesFavorite.add(
                    RulesFavorite(
                        departureCountry,
                        listOf(),
                        arrivalCountry.nameCode,
                        showGenericMessage = false,
                        checkColorRules = true
                    )
                )
            }

        } else {
            rulesForVerification.listRulesFavorite.add(RulesFavorite(departureCountry, rules, arrivalCountry.nameCode))
        }
    }
    private fun getRulesDeparture(
        controlTime: ZonedDateTime,
        departureCountry: Country,
        arrivalCountry: Country,
        issuingCountry: String,
        engineCertificateType: CertificateType,
        rulesForVerification: RulesForVerification,
    ) {
        val rulesUseCase = EngineManager.rulesUseCase


        // SI Pays de départ (lieu de contrôle) != Vert ALORS ‘Message alerte’
        if(SynchronisationUtils.isCountryNotGreen(departureCountry.nameCode)) {

            rulesForVerification.showAlert = true

            // SI présence d’une règle spécifique [Pays de départ]/[Pays de destination]
            var rules = rulesUseCase.invoke(
                controlTime,
                departureCountry.nameCode,
                arrivalCountry.nameCode,
                issuingCountry,
                engineCertificateType
            )

            // SINON SI [Pays de destination Territoire France]
            if(rules.isEmpty()
                && arrivalCountry.isNational) {
                rules = rulesUseCase.invoke(
                    controlTime,
                    departureCountry.nameCode,
                    CODE_FRENCH_TERRITORIES,
                    issuingCountry,
                    engineCertificateType
                )
            }

            // SINON Réponse Bleu hors moteur
            rulesForVerification.listRulesFavorite.add(
                RulesFavorite(
                    arrivalCountry,
                    rules,
                    arrivalCountry.nameCode,
                    showGenericMessage = rules.isEmpty()
                ))

        } else {
            // PAYS VERT

            // SI présence d’une règle spécifique [Pays de départ]/[Pays de destination]
            var rules = rulesUseCase.invoke(
                controlTime,
                departureCountry.nameCode,
                arrivalCountry.nameCode,
                issuingCountry,
                engineCertificateType
            )

            // SINON SI [Pays de destination Territoire France]
            if(rules.isEmpty()
                && arrivalCountry.isNational) {
                rules = rulesUseCase.invoke(
                    controlTime,
                    departureCountry.nameCode,
                    CODE_FRENCH_TERRITORIES,
                    issuingCountry,
                    engineCertificateType
                )
            }

            //SINON SI Pays de destination présent dans le moteur
            if(rules.isEmpty()) {
                rules = rulesUseCase.invoke(
                    controlTime,
                    "",
                    arrivalCountry.nameCode,
                    issuingCountry,
                    engineCertificateType
                )
            }

            //SINON SI Pays de destination ="Autre" ALORS Réponse Bleu hors moteur
            if(rules.isEmpty()
                && arrivalCountry.nameCode == CODE_OTHER_TERRITORY) {
                rulesForVerification.listRulesFavorite.add(
                    RulesFavorite(arrivalCountry, listOf(),  arrivalCountry.nameCode,true))
            } else {

                // SINON réponse bleu hors moteur
                rulesForVerification.listRulesFavorite.add(
                    RulesFavorite(
                        arrivalCountry,
                        rules,
                        arrivalCountry.nameCode,
                        showGenericMessage = rules.isEmpty()
                    )
                )
            }
        }
    }

}