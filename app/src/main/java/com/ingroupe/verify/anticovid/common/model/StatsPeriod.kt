package com.ingroupe.verify.anticovid.common.model

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants
import java.time.LocalDate

class StatsPeriod {

    companion object {
        private const val TAG = "StatsPeriod"
        private const val STATS_PERIOD = "STATS_PERIOD"

        fun getStatsPeriod(): StatsPeriod {
            val certPref = App.getContext().getSharedPreferences(Constants.LOCAL_STAT_KEY, Context.MODE_PRIVATE)
            val jsonMap = certPref.getString(STATS_PERIOD, "")
            val statsPeriod = StatsPeriod()

            if(jsonMap != "") {
                val gson = Gson()
                mutableMapOf<Long, StatsDay>()
                val itemType = object : TypeToken<MutableMap<Long, StatsDay>>() {}.type
                val savedMap: MutableMap<Long, StatsDay> = gson.fromJson(jsonMap, itemType)
                statsPeriod.map = savedMap
            }

            return  statsPeriod
        }

    }

    var map: MutableMap<Long, StatsDay> = mutableMapOf()

    fun getStatsDay(date: LocalDate): StatsDay {
        val key = date.toEpochDay()

        var statsDay =  map[key]

        if(statsDay == null) {
            statsDay = StatsDay()
            map[key] = statsDay
        }

        return  statsDay
    }

    private fun saveStatsPeriod() {
        val gson = Gson()
        val newJsonMap = gson.toJson(map)

        val certPref = App.getContext().getSharedPreferences(Constants.LOCAL_STAT_KEY, Context.MODE_PRIVATE)
        with(certPref.edit()) {
            putString(STATS_PERIOD, newJsonMap)
            apply()
        }
    }

    fun cleanOldStatsAndSave() {

        val newMap = map.filterKeys { epochDay ->
            LocalDate.ofEpochDay(epochDay).plusDays(15L).isAfter(LocalDate.now())
        }

        map = newMap.toMutableMap()

        saveStatsPeriod()
    }


}