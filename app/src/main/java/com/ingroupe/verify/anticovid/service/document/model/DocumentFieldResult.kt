package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DocumentFieldResult(
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("label")
    var label: String? = null,
    @SerializedName("value")
    var value: String? = null
) : Serializable