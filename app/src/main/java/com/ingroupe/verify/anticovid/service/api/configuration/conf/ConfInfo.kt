package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfInfo(
    @SerializedName("validityValuesFormat")
    var validityValuesFormat: Map<String, String>? = null
) : Serializable