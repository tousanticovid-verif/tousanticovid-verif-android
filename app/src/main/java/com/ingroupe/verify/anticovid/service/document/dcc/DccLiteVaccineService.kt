package com.ingroupe.verify.anticovid.service.document.dcc

import android.content.Context
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Constants.ValidityValues.*
import com.ingroupe.verify.anticovid.common.Constants.ValidityValuesString.*
import com.ingroupe.verify.anticovid.service.document.BarcodeService
import com.ingroupe.verify.anticovid.service.document.dcc.preparation.*
import com.ingroupe.verify.anticovid.service.document.model.DocumentStaticDccResult
import com.ingroupe.verify.anticovid.service.document.model.InfoTestForStat
import java.time.*
import java.time.temporal.ChronoUnit

object DccLiteVaccineService : DccModeInterface {


    override fun getDccTest(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?,
        infoTestForStat: InfoTestForStat
    ) {
        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.tests?.get(0)?.let { test ->
            TestPreparation.getPreparation(test, mappedDynamicData, infoTestForStat)?.let { prep ->


                if (staticDcc.hasValidSignature
                    && (prep.isPCR || prep.isAntigenic)
                    && DccModeInterface.checkIat(
                        staticDcc.issuedAt,
                        prep.currentDate.toZonedDateTime()
                    )
                    && DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_TEST,
                        prep.currentDate.toZonedDateTime()
                    )
                ) {
                    val dob =
                        DccModeInterface.getLocalDateFromDcc(staticDcc.dccData!!.greenCertificate.dateOfBirth)
                    val dobAfterPeriodTest = if (dob == null) {
                        prep.currentDate.toLocalDate().plusDays(1)
                    } else {
                        dob.plus(Period.parse(getVVS(TEST_ACCEPTANCE_AGE_PERIOD)))
                    }
                    val dobAfterPeriodRevovery = if (dob == null) {
                        prep.currentDate.toLocalDate().plusDays(1)
                    } else {
                        dob.plus(Period.parse(getVVS(RECOVERY_ACCEPTANCE_AGE_PERIOD)))
                    }
                    val hoursSinceTest = ChronoUnit.HOURS.between(prep.testDate, prep.currentDate)
                    val daysSinceTest = ChronoUnit.DAYS.between(prep.testDate.truncatedTo(ChronoUnit.DAYS), prep.currentDate)


                    if (test.testResult.trim() == TestPreparation.RESULT_TEST_NEGATIVE
                        && (
                            (prep.isPCR
                                && hoursSinceTest < getVV(TEST_NEGATIVE_PCR_END_HOUR)
                            )
                            || (prep.isAntigenic
                                && hoursSinceTest < getVV(TEST_NEGATIVE_ANTIGENIC_END_HOUR)
                            )
                        )
                        && (
                            prep.currentDate.toLocalDate() < dobAfterPeriodTest
                            || test.testingCentre.startsWith("PV")
                        )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK
                        validityGlobalStatus = context.getString(R.string.result_valid)
                    } else if (test.testResult.trim() == TestPreparation.RESULT_TEST_POSITIVE
                        && (
                            (prep.isPCR
                                && daysSinceTest >= getVV(TEST_POSITIVE_PCR_START_DAY)
                            )
                            || (prep.isAntigenic
                                && daysSinceTest >= getVV(TEST_POSITIVE_ANTIGENIC_START_DAY)
                            )
                        )
                        && (
                            (prep.currentDate.toLocalDate() >= dobAfterPeriodRevovery
                                && (
                                    (test.testingCentre.startsWith("TV")
                                            && daysSinceTest <= getVV(RECOVER_DELAY_MAX_TV)
                                    )
                                    || (!test.testingCentre.startsWith("TV")
                                        && daysSinceTest <= getVV(RECOVER_DELAY_MAX)
                                    )
                                )
                            )
                            || (prep.currentDate.toLocalDate() < dobAfterPeriodRevovery
                                && daysSinceTest <= getVV(RECOVER_DELAY_MAX_UNDER_AGE)
                            )
                        )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK
                        validityGlobalStatus = context.getString(R.string.result_valid)
                    }
                }
            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }


    override fun getDccVaccination(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.vaccinations?.get(0)?.let { vac ->

            VaccinationPreparation.getPreparation(vac, mappedDynamicData)?.let { prep ->

                if (!staticDcc.hasValidSignature
                    || !DccModeInterface.checkIat(staticDcc.issuedAt, ZonedDateTime.now())
                    || !DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_VACCINATION,
                        ZonedDateTime.now()
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.result_invalid)
                } else if (vac.doseNumber >= vac.totalSeriesOfDoses
                    && prep.medicinalProductFound
                    && prep.manufacturerFound
                    && prep.vaccineFound
                ) {
                    val dob =
                        DccModeInterface.getLocalDateFromDcc(staticDcc.dccData!!.greenCertificate.dateOfBirth)
                    val dobAfterPeriod = if (dob == null) {
                        prep.currentDate.plusDays(1)
                    } else {
                        dob.plus(Period.parse(getVVS(VACCINE_BOOSTER_AGE_PERIOD)))
                    }

                    if (vac.medicinalProduct.uppercase()
                            .trim() == VaccinationPreparation.VACCINE_JANSSEN
                        && vac.totalSeriesOfDoses == 1
                        && !prep.currentDate.isBefore(
                            prep.vacDate.plusDays(getVV(VACCINE_DELAY_JANSSEN_1).toLong())
                        )
                        && !prep.currentDate.isAfter(
                            prep.vacDate.plusDays(getVV(VACCINE_DELAY_MAX_JANSSEN_1).toLong())
                        )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK
                        validityGlobalStatus = context.getString(R.string.result_valid)

                    } else if (prep.currentDate < dobAfterPeriod
                        && (
                            (
                                (vac.totalSeriesOfDoses == 1
                                    && !prep.currentDate.isBefore(
                                        prep.vacDate.plusDays(getVV(VACCINE_DELAY).toLong())
                                    )
                                ) && vac.medicinalProduct.uppercase().trim() != VaccinationPreparation.VACCINE_JANSSEN
                            )
                            || (vac.totalSeriesOfDoses == 2
                                    && !prep.currentDate.isBefore(
                                        prep.vacDate.plusDays(getVV(VACCINE_DELAY).toLong())
                                    )
                                )
                            || (vac.totalSeriesOfDoses >= 3
                                    && (!prep.currentDate.isBefore(
                                            prep.vacDate.plusDays(
                                                getVV(VACCINE_BOOSTER_DELAY_UNDER_AGE_NEW).toLong()
                                            )
                                        )
                                    )
                                )
                            )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK
                        validityGlobalStatus = context.getString(R.string.result_valid)

                    } else if (prep.currentDate >= dobAfterPeriod
                        &&
                        (
                            ( vac.medicinalProduct.uppercase()
                                    .trim() != VaccinationPreparation.VACCINE_JANSSEN
                                && vac.medicinalProduct.uppercase()
                                    .trim() != VaccinationPreparation.VACCINE_NOVA
                                && (
                                    (vac.totalSeriesOfDoses == 1
                                        && vac.doseNumber == 1
                                        && !prep.currentDate.isBefore(
                                            prep.vacDate.plusDays(getVV(VACCINE_DELAY).toLong())
                                        )
                                        && !prep.currentDate.isAfter(
                                            prep.vacDate.plusDays(getVV(VACCINE_DELAY_MAX_RECOVERY).toLong())
                                        )
                                    )
                                    || (vac.totalSeriesOfDoses == 2
                                        && vac.doseNumber == 2
                                        && !prep.currentDate.isBefore(
                                            prep.vacDate.plusDays(getVV(VACCINE_DELAY).toLong())
                                        )
                                        && !prep.currentDate.isAfter(
                                            prep.vacDate.plusDays(getVV(VACCINE_DELAY_MAX).toLong())
                                        )
                                    )
                                    || (
                                        (vac.totalSeriesOfDoses >= 3
                                            || vac.doseNumber > vac.totalSeriesOfDoses
                                        )
                                        && !prep.currentDate.isAfter(
                                            prep.vacDate.plusDays(getVV(VACCINE_BOOSTER_DELAY_MAX).toLong())
                                        )
                                        && (!prep.currentDate.isBefore(
                                                prep.vacDate.plusDays(
                                                    getVV(VACCINE_BOOSTER_DELAY_NEW).toLong()
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                            ||
                            (
                                vac.medicinalProduct.uppercase()
                                    .trim() == VaccinationPreparation.VACCINE_NOVA
                                && (
                                    (vac.totalSeriesOfDoses == 1
                                        && vac.doseNumber == 1
                                        && !prep.currentDate.isBefore(
                                            prep.vacDate.plusDays(getVV(VACCINE_DELAY_NOVAVAX).toLong())
                                    )
                                    && !prep.currentDate.isAfter(
                                        prep.vacDate.plusDays(getVV(VACCINE_DELAY_MAX_RECOVERY_NOVAVAX).toLong())
                                        )
                                    )
                                    || (vac.totalSeriesOfDoses == 2
                                            && vac.doseNumber == 2
                                            && !prep.currentDate.isBefore(
                                                prep.vacDate.plusDays(getVV(VACCINE_DELAY_NOVAVAX).toLong())
                                            )
                                            && !prep.currentDate.isAfter(
                                                prep.vacDate.plusDays(getVV(VACCINE_DELAY_MAX_NOVAVAX).toLong())
                                                )
                                            )
                                    || (
                                        (vac.totalSeriesOfDoses >= 3
                                            || vac.doseNumber > vac.totalSeriesOfDoses)
                                        && (!prep.currentDate.isBefore(
                                            prep.vacDate.plusDays(
                                                getVV(VACCINE_DELAY_NOVAVAX).toLong()
                                            )
                                        )
                                        && !prep.currentDate.isAfter(
                                            prep.vacDate.plusDays(getVV(VACCINE_BOOSTER_DELAY_MAX_NOVAVAX).toLong())
                                            )
                                        )
                                    )
                                )
                            )
                            ||
                            (
                                vac.medicinalProduct.uppercase()
                                    .trim() == VaccinationPreparation.VACCINE_JANSSEN
                                && (
                                    (vac.totalSeriesOfDoses == 2
                                            && vac.doseNumber == 2
                                            && !prep.currentDate.isBefore(
                                                prep.vacDate.plusDays(getVV(VACCINE_DELAY_JANSSEN_2).toLong())
                                            )
                                            && !prep.currentDate.isAfter(
                                                prep.vacDate.plusDays(getVV(VACCINE_DELAY_MAX_JANSSEN_2).toLong())
                                                )
                                            )
                                    || (
                                        (vac.totalSeriesOfDoses >= 3
                                            || vac.doseNumber > vac.totalSeriesOfDoses)
                                        && (!prep.currentDate.isBefore(
                                            prep.vacDate.plusDays(
                                                getVV(VACCINE_DELAY_JANSSEN_2).toLong()
                                            )
                                        )
                                        && !prep.currentDate.isAfter(
                                            prep.vacDate.plusDays(getVV(VACCINE_BOOSTER_DELAY_MAX_JANSSEN).toLong())
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK
                        validityGlobalStatus = context.getString(R.string.result_valid)

                    } else {
                        validityGlobal = Constants.GlobalValidity.KO
                        validityGlobalStatus = context.getString(R.string.result_invalid)
                    }
                } else {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.result_invalid)
                }
            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    override fun getDccRecovery(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?,
        infoTestForStat: InfoTestForStat
    ) {

        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.recoveryStatements?.get(0)?.let { statement ->
            RecoveryPreparation.getPreparation(statement, infoTestForStat)?.let { prep ->

                val dob =
                    DccModeInterface.getLocalDateFromDcc(staticDcc.dccData!!.greenCertificate.dateOfBirth)
                val dobAfterPeriodRevovery = if (dob == null) {
                    prep.currentDate.plusDays(1)
                } else {
                    dob.plus(Period.parse(getVVS(RECOVERY_ACCEPTANCE_AGE_PERIOD)))
                }
                val daysSinceTest = ChronoUnit.DAYS.between(prep.PositiveTestDate, prep.currentDate)

                if (staticDcc.hasValidSignature
                    && daysSinceTest >= getVV(RECOVERY_START_DAY)
                    && DccModeInterface.checkIat(staticDcc.issuedAt, ZonedDateTime.now())
                    && DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_RECOVERY,
                        prep.currentDate.atStartOfDay(ZoneOffset.systemDefault())
                    )
                    && (
                        (prep.currentDate >= dobAfterPeriodRevovery
                            && (
                                (statement.certificateIssuer.startsWith("TV")
                                    && daysSinceTest <= getVV(RECOVER_DELAY_MAX_TV)
                                )
                                || (!statement.certificateIssuer.startsWith("TV")
                                    && daysSinceTest <= getVV(RECOVER_DELAY_MAX)
                                )
                            )
                        )
                        || (prep.currentDate < dobAfterPeriodRevovery
                            && daysSinceTest <= getVV(RECOVER_DELAY_MAX_UNDER_AGE)
                        )
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.OK
                    validityGlobalStatus = context.getString(R.string.result_valid)
                } else {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.result_invalid)
                }
            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    override fun getDccExemption(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.exemption?.let { exemption ->
            ExemptionPreparation.getPreparation(exemption)?.let { prep ->

                val currentDate = LocalDate.now()

                if (staticDcc.hasValidSignature
                    && (currentDate.isAfter(prep.dateFrom) || currentDate.isEqual(prep.dateFrom))
                    && (currentDate.isBefore(prep.dateUntil) || currentDate.isEqual(prep.dateUntil))
                    && DccModeInterface.checkIat(staticDcc.issuedAt, ZonedDateTime.now())
                    && DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_EXEMPTION,
                        ZonedDateTime.now()
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.OK
                    validityGlobalStatus = context.getString(R.string.result_valid)
                } else {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.result_invalid)
                }


            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    override fun getDccActivity(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.activity?.let { activity ->
            ActivityPreparation.getPreparation(activity)?.let { prep ->

                if (staticDcc.hasValidSignature
                    && prep.expirationTime.isAfter(ZonedDateTime.now())
                    && prep.issuedAt.isBefore(ZonedDateTime.now())
                ) {
                    validityGlobal = Constants.GlobalValidity.OK
                    validityGlobalStatus = context.getString(R.string.result_valid)
                } else {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.result_invalid)
                }
            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }


    private fun getVV(svEnum : Constants.ValidityValues): Int {
        return BarcodeService.getVV(svEnum, Constants.ControlMode.VACCINE)
    }

    private fun getVVS(svEnum : Constants.ValidityValuesString): String {
        return BarcodeService.getVVS(svEnum, Constants.ControlMode.VACCINE)
    }
}