package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncValidityMode (
    @SerializedName("testNegativePcrEndHour")
    var testNegativePcrEndHour: Int? = null,
    @SerializedName("testNegativeAntigenicEndHour")
    var testNegativeAntigenicEndHour: Int? = null,
    @SerializedName("testPositivePcrStartDay")
    var testPositivePcrStartDay: Int? = null,
    @SerializedName("testPositivePcrEndDay")
    var testPositivePcrEndDay: Int? = null,
    @SerializedName("testPositiveAntigenicStartDay")
    var testPositiveAntigenicStartDay: Int? = null,
    @SerializedName("testPositiveAntigenicEndDay")
    var testPositiveAntigenicEndDay: Int? = null,
    @SerializedName("testAcceptanceAgePeriod")
    var testAcceptanceAgePeriod: String? = null,
    @SerializedName("recoveryStartDay")
    var recoveryStartDay: Int? = null,
    @SerializedName("recoveryEndDay")
    var recoveryEndDay: Int? = null,
    @SerializedName("vaccineDelay")
    var vaccineDelay: Int? = null,

    @SerializedName("recoveryDelayMaxTV")
    var recoveryDelayMaxTV: Int? = null,
    @SerializedName("recoveryDelayMax")
    var recoveryDelayMax: Int? = null,
    @SerializedName("recoveryDelayMaxUnderAge")
    var recoveryDelayMaxUnderAge: Int? = null,
    @SerializedName("recoveryAcceptanceAgePeriod")
    var recoveryAcceptanceAgePeriod: String? = null,

    @SerializedName("vaccineDelayMax")
    var vaccineDelayMax: Int? = null,
    @SerializedName("vaccineDelayMaxRecovery")
    var vaccineDelayMaxRecovery: Int? = null,
    @SerializedName("vaccineDelayJanssen1")
    var vaccineDelayJanssen1: Int? = null,
    @SerializedName("vaccineDelayMaxJanssen1")
    var vaccineDelayMaxJanssen1: Int? = null,
    @SerializedName("vaccineBoosterDelayNew")
    var vaccineBoosterDelayNew: Int? = null,
    @SerializedName("vaccineBoosterDelayMax")
    var vaccineBoosterDelayMax: Int? = null,
    @SerializedName("vaccineBoosterDelayUnderAgeNew")
    var vaccineBoosterDelayUnderAgeNew: Int? = null,
    @SerializedName("vaccineBoosterAgePeriod")
    var vaccineBoosterAgePeriod: String? = null,

    @SerializedName("vaccineDelayNovavax")
    var vaccineDelayNovavax: Int? = null,
    @SerializedName("vaccineDelayMaxRecoveryNovavax")
    var vaccineDelayMaxRecoveryNovavax: Int? = null,
    @SerializedName("vaccineDelayMaxNovavax")
    var vaccineDelayMaxNovavax: Int? = null,
    @SerializedName("vaccineBoosterDelayMaxNovavax")
    var vaccineBoosterDelayMaxNovavax: Int? = null,
    @SerializedName("vaccineDelayJanssen2")
    var vaccineDelayJanssen2: Int? = null,
    @SerializedName("vaccineDelayMaxJanssen2")
    var vaccineDelayMaxJanssen2: Int? = null,
    @SerializedName("vaccineBoosterDelayMaxJanssen")
    var vaccineBoosterDelayMaxJanssen: Int? = null,
)