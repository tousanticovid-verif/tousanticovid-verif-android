package com.ingroupe.verify.anticovid.ui.actionchoice

import com.ingroupe.verify.anticovid.common.Constants

interface ActionChoicePresenter {
    fun getSavedTravelType() : Constants.TravelType
}