package com.ingroupe.verify.anticovid.common

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.ui.actionchoice.ActionChoiceChildFragment
import com.ingroupe.verify.anticovid.ui.init.InitChildFragment
import com.ingroupe.verify.anticovid.ui.scan.ScanChildFragment
import java.io.Serializable
import java.util.*

abstract class FeatureFragment : Fragment() {

    companion object {
        const val TAG = "FeatureFragment"
        protected const val NO_FRAGMENT_TAG = "NO_FRAGMENT"
        private const val CURRENT_TAG_KEY = "CURRENT_TAG"
        private const val TAG_STACK_KEY = "TAG_STACK"
    }

    private var currentTag: String? = NO_FRAGMENT_TAG
    private var tagToPop: String? = null
    private var currentBundle: Bundle? = null
    private var forceReplace = false

    private lateinit var tagStack: Stack<String>
    private var currentFragment: FeatureChildFragment? = null

    override fun onResume() {
        checkCurrentTag()
        super.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        currentTag = savedInstanceState?.getSerializable(CURRENT_TAG_KEY) as? String
        @Suppress("UNCHECKED_CAST")
        tagStack = savedInstanceState?.getSerializable(TAG_STACK_KEY) as? Stack<String> ?: Stack()
        if (tagStack.isEmpty()) {
            tagStack.add(NO_FRAGMENT_TAG)
        }

        if (NO_FRAGMENT_TAG == currentTag) {
            replaceFragment(getFirstTag())
        } else {

            val model = activity?.run {
                ViewModelProvider(this).get(SharedViewModel::class.java)
            } ?: throw Exception("Invalid Activity")

            if(model.reloadConfiguration) {
                Log.d(TAG, "reloadConfiguration")
                model.reloadConfiguration = false
                model.configuration = null
                if(tagStack.contains(ActionChoiceChildFragment.TAG)) {
                    popToTag(ActionChoiceChildFragment.TAG)
                } else {
                    replaceFragment(currentTag ?: getFirstTag())
                }
            } else {
                replaceFragment(currentTag ?: getFirstTag())
            }
        }

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    /**
     *
     * @param tag
     * @param args
     * @return true if fragment is created, false if fragment already exists
     */
    open fun replaceFragment(tag: String, vararg args: Serializable, sharedElement: View? = null): Boolean {
        if (tag == currentTag && !forceReplace) {
            Log.d("FRAGMENTS", "Fragment $currentTag is already present")
            if (currentFragment == null && isAdded) {
                currentFragment = childFragmentManager.findFragmentByTag(currentTag) as? FeatureChildFragment
                currentFragment?.featureFragment = this
            }
            initFeatureFragment()
            return false
        }

        if(!isAdded) {
            return false
        }

        val fragmentCreated = (childFragmentManager.findFragmentByTag(tag) == null)
        val fragment = childFragmentManager.findFragmentByTag(tag) ?: createFragment(tag, args)
        val transaction = childFragmentManager.beginTransaction().replace(getLayoutId(), fragment, tag)
        if (fragmentCreated) transaction.addToBackStack(tag)
        if (sharedElement != null) transaction.addSharedElement(
            sharedElement,
            ViewCompat.getTransitionName(sharedElement)!!
        )
        transaction.commitAllowingStateLoss()


        currentTag = tag
        tagStack.add(currentTag)

        currentFragment = fragment as? FeatureChildFragment
        currentFragment?.featureFragment = this

        initFeatureFragment()

        activity?.invalidateOptionsMenu()

        return fragmentCreated
    }

    private fun setActivityTitle(title: String) {
        activity?.title = title
    }

    private fun setActivityTitle(id: Int) {
        if (id != 0) {
            activity?.let {
                it.title = it.getString(id)
            }
        }
    }

    private fun setSoftInputMode(softInputMode: Int) {
        activity?.window?.setSoftInputMode(softInputMode)
    }

    abstract fun createFragment(tag: String, args: Array<out Serializable>): Fragment

    abstract fun getLayoutId(): Int

    abstract fun getFirstTag(): String

    private fun initFeatureFragment() {
        getTitle()?.let { setActivityTitle(it) }
        //si un titre a été défini par ID, on l'utilise car peut être traduit
        currentFragment?.getTitleId()?.let { setActivityTitle(it) }
        currentFragment?.getSoftInputMode()?.let { setSoftInputMode(it) }
        activity?.let { (it as MainActivity).hideKeyboard() }
    }

    fun getTitle(): String? = currentFragment?.getTitle()

    /**
     *
     * @return true if the back pressed has been consumed
     */
    open fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentByTag(currentTag)

        if ((fragment is FeatureChildFragment)) {
            val canPressBack = fragment.canPressBack()
            if (canPressBack) {
                if (tagStack.isEmpty()) {
                    return false
                }
                if (tagStack.size <= 1) {
                    return false
                }
                val tag = tagStack[tagStack.size - 2]
                if (tag == NO_FRAGMENT_TAG) {
                    return false
                }
                pop()
            }
            return true
        } else {
            return false
        }
    }

    private fun pop() {
        tagStack.pop()
        childFragmentManager.popBackStack()
        currentTag = tagStack.peek()
        val fragment = childFragmentManager.findFragmentByTag(currentTag)
        if (fragment is FeatureChildFragment) {
            currentFragment = fragment
            currentFragment?.featureFragment = this
        }
        initFeatureFragment()
        activity?.invalidateOptionsMenu()

    }

    fun popToTag(tag: String, bundle: Bundle? = null, force: Boolean = false) {
        Log.d(TAG, "pop to Tag")
        try {
            val fragment = childFragmentManager.findFragmentByTag(tag)
            if (fragment != null) {
                bundle?.apply {
                    fragment.arguments = bundle
                }
                childFragmentManager.popBackStackImmediate(tag, 0)
                while (currentTag != tag && currentTag != NO_FRAGMENT_TAG) {
                    tagStack.pop()
                    if (tagStack.empty()) {
                        currentTag = tag
                        tagStack.add(currentTag)
                    } else {
                        currentTag = tagStack.peek()
                    }
                }

                if (fragment is FeatureChildFragment) {
                    currentFragment = fragment
                    currentFragment?.featureFragment = this
                }
                initFeatureFragment()

            } else if (force) {
                tagStack.clear()
                childFragmentManager.popBackStackImmediate()
                replaceFragment(tag)
            }
        } catch (e: IllegalStateException) {
            tagToPop = tag
            currentBundle = bundle
            forceReplace = true
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(CURRENT_TAG_KEY, currentTag)
        outState.putSerializable(TAG_STACK_KEY, tagStack)
        super.onSaveInstanceState(outState)
    }

    private fun checkCurrentTag() {
        Log.d(TAG, "check Current Tag")
        tagToPop?.let {
            if (tagStack.isEmpty() || tagStack.peek() != it) {
                popToTag(it, currentBundle, force = true)
            } else if (tagStack.peek() == it) {
                val fragment = childFragmentManager.findFragmentByTag(currentTag)
                currentBundle?.let { bundle -> fragment?.arguments = bundle }
            }
            tagToPop = null
            currentBundle = null
            forceReplace = false
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return currentFragment?.onOptionsItemSelected(item) ?: true
    }

    fun showNavigation(): MainActivity.NavigationIcon? {
        return currentFragment?.showNavigation()
    }

    fun forceToActionChoice() {
        Log.d(TAG, "forceToActionChoice")
        if(tagStack.contains(ScanChildFragment.TAG)) {
            popToTag(ActionChoiceChildFragment.TAG)
        }
    }

    fun getCurrentTag() : String? {
        return currentTag
    }

    //Used to refresh the ActionChoiceFragment
    fun refreshFragment() {
        val fragment = childFragmentManager.findFragmentByTag(currentTag)
        if (fragment != null && currentTag != null) {
            val tag = currentTag!!
            popToTag(InitChildFragment.TAG)
            replaceFragment(tag)
        }

    }

    fun isScanBlocked() : Boolean {
       return currentFragment?.isScanBlocked() ?: false
    }
}