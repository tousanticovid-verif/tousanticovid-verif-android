package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncRuleEngine(
    @SerializedName("messageOtModeFR")
    var messageOtModeFR: String? = null
)