package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DocumentSignatureResult(
    @SerializedName("isValid")
    var isValid: Boolean? = null,
    @SerializedName("status")
    var status: String? = null

) : Serializable