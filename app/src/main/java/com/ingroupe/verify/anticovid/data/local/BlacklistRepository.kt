package com.ingroupe.verify.anticovid.data.local

import android.util.Log
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.data.local.blacklist.BlacklistDataSource
import com.ingroupe.verify.anticovid.service.api.configuration.blacklist.BlacklistResult
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import com.ingroupe.verify.anticovid.synchronization.elements.Blacklist
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
class BlacklistRepository(private val blacklistDataSource: BlacklistDataSource) {

    companion object {
        const val TAG = "BlacklistRepository"
    }


    fun getLastIndex(blacklistType: Blacklist.BlacklistType): Int {
        val list = blacklistDataSource.getLastBlacklist(blacklistType)
        return if (blacklistDataSource.getLastBlacklist(blacklistType).isEmpty()) 0 else list[0].id
    }


    fun loadBlacklistPageWithCondition() {
        CoroutineScope(Dispatchers.IO).launch {
            loadBlacklistTypePageWithCondition(Blacklist.BlacklistType.BLACKLIST_DCC)
            loadBlacklistTypePageWithCondition(Blacklist.BlacklistType.BLACKLIST_2DDOC)
        }
    }

    private fun loadBlacklistTypePageWithCondition(blacklistType: Blacklist.BlacklistType) {
        CoroutineScope(Dispatchers.IO).launch {
            while (Blacklist.isBlacklistSyncToDo(blacklistType)) {
                loadBlacklistPage(blacklistType)
            }
        }
    }

    fun loadBlacklistPageWithoutCondition() {
        CoroutineScope(Dispatchers.IO).launch {
            while (!Blacklist.isBlacklistUpToDate(Blacklist.BlacklistType.BLACKLIST_DCC)) {
                loadBlacklistPage(Blacklist.BlacklistType.BLACKLIST_DCC)
            }
            while (!Blacklist.isBlacklistUpToDate(Blacklist.BlacklistType.BLACKLIST_2DDOC)) {
                loadBlacklistPage(Blacklist.BlacklistType.BLACKLIST_2DDOC)
            }
        }
    }

    private fun loadBlacklistPage(blacklistType: Blacklist.BlacklistType) {
        val blResult: BlacklistResult = ConfServiceUtils.getBlacklistPage(blacklistType, getLastIndex(blacklistType))

        blacklistDataSource.addBlacklist(blacklistType,blResult.elements?: emptyList())
        Blacklist.saveLastIndexBlacklist(blacklistType, blResult.lastIndexBlacklist)

        if(blResult.elements.isNullOrEmpty()) {
            Log.d(TAG,
                "lastBackIndex:" + blResult.lastIndexBlacklist
                        + " / lastIndexSent:aucun"
            )
        } else {
            Log.d(TAG,
                "lastBackIndex:" + blResult.lastIndexBlacklist
                        + " / lastIndexSent:" + blResult.elements?.last()?.id
            )
        }
    }


    fun deleteFromId(blacklistType: Blacklist.BlacklistType, id: Int) = blacklistDataSource.deleteFromId(blacklistType, id)

    fun isHashBlacklisted(blacklistType: Blacklist.BlacklistType, hash: String): Boolean = blacklistDataSource.isHashBlacklisted(blacklistType, hash)


    fun initDatas() : Boolean {
        val blDcc : BlacklistResult = Utils.loadFromAsset("sync/sync_bl_dcc.json")
            ?: return false

        blacklistDataSource.addBlacklist(Blacklist.BlacklistType.BLACKLIST_DCC,blDcc.elements?: emptyList())
        Blacklist.saveLastIndexBlacklist(Blacklist.BlacklistType.BLACKLIST_DCC, blDcc.lastIndexBlacklist)


        val bl2ddoc : BlacklistResult = Utils.loadFromAsset("sync/sync_bl_2ddoc.json")
            ?: return false

        blacklistDataSource.addBlacklist(Blacklist.BlacklistType.BLACKLIST_2DDOC,bl2ddoc.elements?: emptyList())
        Blacklist.saveLastIndexBlacklist(Blacklist.BlacklistType.BLACKLIST_2DDOC, bl2ddoc.lastIndexBlacklist)

        return true
    }

}