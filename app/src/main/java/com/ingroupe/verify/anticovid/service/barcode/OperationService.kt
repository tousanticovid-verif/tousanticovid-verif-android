package com.ingroupe.verify.anticovid.service.barcode

import android.util.Log
import com.ingroupe.verify.anticovid.service.barcode.enums.ErrorBarcodeEnum
import com.ingroupe.verify.anticovid.service.barcode.enums.OperationEnum
import org.apache.commons.codec.binary.Base32
import java.nio.charset.StandardCharsets
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

private const val DATE_TIME_PATTERN: String = "dd/MM/yyyy"

abstract class OperationService {

    companion object {
        const val TAG = "OperationService"

        fun transformFieldValue(input: String, operation: OperationEnum?): String {
            var value = input
            if (value.isEmpty() || "FFFF" == value) {
                return ""
            }
            when (operation) {
                OperationEnum.CONVERT_HEX_TO_DATE -> value = convertHexToDate(value)
                OperationEnum.CONVERT_STRING_TO_DATE -> value = convertStringToDate(value)
                OperationEnum.DECODE_BASE16 -> value = Integer.valueOf(value, 16).toString()
                OperationEnum.DECODE_BASE32 -> value =
                        String(Base32().decode(value), StandardCharsets.UTF_8)
                OperationEnum.DECODE_BASE36 -> value = Integer.valueOf(value, 36).toString()
                OperationEnum.GET_COUNTRY -> value = getCountry(value)
                OperationEnum.REMOVE_SLASH -> value = value.replace("/".toRegex(), " ")
                OperationEnum.TRANSLATE_BOOLEAN -> value = translateBoolean(value)
                else -> {
                }
            }
            return value
        }

        private fun convertHexToDate(value: String): String {
            return try {
                val days = value.toLong(16)
                val date = LocalDate.of(2000, 1, 1).plusDays(days)
                date.format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN))
            } catch (nfe: NumberFormatException) {
                Log.i(TAG, ErrorBarcodeEnum.ERR04.message)
                value
            }
        }

        private fun convertStringToDate(value: String): String {
            if (value.length == 8) {
                return StringBuilder(value)
                    .insert(4, '/')
                    .insert(2, '/')
                    .toString()
            } else if (value.length == 12) {
                return StringBuilder(value)
                    .insert(10, ':')
                    .insert(8, ' ')
                    .insert(4, '/')
                    .insert(2, '/')
                    .toString()
            }
            Log.i(TAG, ErrorBarcodeEnum.ERR04.message)
            return value
        }

        fun getCountry(value: String): String {
            val country: String = Locale("FR", value).displayCountry
            if (value.equals(country, ignoreCase = true)) {
                Log.i(TAG, ErrorBarcodeEnum.ERR05.message)
            }
            return country
        }

        private fun translateBoolean(value: String): String {
            if ("0" == value) {
                return "FAUX"
            } else if ("1" == value) {
                return "VRAI"
            }
            Log.i(TAG, ErrorBarcodeEnum.ERR05.message)
            return value
        }
    }

}