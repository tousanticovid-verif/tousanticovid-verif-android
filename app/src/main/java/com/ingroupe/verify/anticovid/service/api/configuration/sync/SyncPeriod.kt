package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SyncPeriod (
    @SerializedName("frequency")
    var frequency: Long? = null,
    @SerializedName("step1")
    var step1: Long? = null,
    @SerializedName("step2")
    var step2: Long? = null,
    @SerializedName("step3")
    var step3: Long? = null
): Serializable
