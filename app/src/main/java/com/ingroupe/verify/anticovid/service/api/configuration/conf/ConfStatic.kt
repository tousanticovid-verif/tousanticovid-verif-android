package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfStatic(
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("label")
    var label: String? = null,
    @SerializedName("order")
    var order: Int? = null
) : Serializable