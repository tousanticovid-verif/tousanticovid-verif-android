package com.ingroupe.verify.anticovid.ui.result

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.*
import android.util.Log
import android.view.*
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.BuildConfig
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.adapter.IAdapterInterfaceClick
import com.ingroupe.verify.anticovid.adapter.ResultScanAdapter
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.*
import com.ingroupe.verify.anticovid.common.model.ResultData
import com.ingroupe.verify.anticovid.common.model.ResultScan
import com.ingroupe.verify.anticovid.databinding.ResultScanMainBinding
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import com.ingroupe.verify.anticovid.ui.actionchoice.ActionChoiceChildFragment
import com.ingroupe.verify.anticovid.ui.scan.ScanChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialresult.lite.TutorialResultLiteHelpChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialresult.premium.TutorialResultPremiumHelpChildFragment
import org.greenrobot.eventbus.EventBus


class ResultScanChildFragment : FeatureChildFragment(), ResultScanView, IAdapterInterfaceClick {


    override fun getTitle(): String = "Résultat"
    override fun getTitleId(): Int = R.string.title_result

    companion object {
        const val TAG = "resultScanF"
        fun newInstance() = ResultScanChildFragment()
        const val TIMEOUT = 30 * 1000L //Time to go back to ActionChoice Page
        const val TIMEOUT_PREMIUM = 60 * 1000L
        const val TIMEOUT_BLACKLIST = 5 * 60 * 1000L //Time to go back to ActionChoice Page
    }
    private var _binding: ResultScanMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var presenter: ResultScanPresenter? = null

    private lateinit var model: SharedViewModel

    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var viewAdapter: androidx.recyclerview.widget.RecyclerView.Adapter<*>
    private lateinit var viewManager: androidx.recyclerview.widget.RecyclerView.LayoutManager

    private var resultScan = ResultScan()
    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = ResultScanMainBinding.inflate(inflater, container, false)
        val view = binding.root

        if (presenter == null) {
            presenter = context?.let { ResultScanPresenterImpl(this) }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(!BuildConfig.WITH_CHECK_HELP) {
            activity?.window?.setFlags(
                WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE
            )
        }

        binding.buttonNewScan.setOnClickListener {
            goToScan()
        }

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        resultScan = ResultScan()

        if (model.currentResponse?.errors?.isNotEmpty() == true) {
            presenter?.prepareErrorResult(resultScan, model.currentResponse?.errors!!, requireContext())
        }

        if (Utils.notNull(model.currentResponse?.data, view, presenter, context)) {

            if(model.configuration == null) {
                presenter?.reloadConf(model)
            }

            presenter!!.prepareResult(
                resultScan,
                model.currentResponse?.resourceType,
                model.currentResponse?.data!!,
                model.configuration,
                model.travelConfiguration,
                requireContext(),
                JWTUtils.getDisplayMode(),
                model.currentResponse?.engineData
            )
        }

        if (Utils.notNull(resultScan, view)) {
            viewManager = androidx.recyclerview.widget.LinearLayoutManager(activity)

            // si le résultat est invalide, on fait vibrer le téléphone
            model.resultAlreadyViewed?.let {
                if (!it) {
                    vibrateIfInvalid()
                }
            }
            model.resultAlreadyViewed = true

            viewAdapter = ResultScanAdapter(
                resultScan,
                this
            )

            recyclerView = binding.recyclerViewResult.apply {
                // use this setting to improve performance if you know that changes
                // in content do not change the tutorial_skip_view size of the RecyclerView
                setHasFixedSize(true)

                // use a linear tutorial_skip_view manager
                layoutManager = viewManager

                // specify an viewAdapter (see also next example)
                adapter = viewAdapter
            }
        }

        handler.postDelayed({
            if(this.featureFragment?.getCurrentTag() == TAG) {
                goToActionChoice()
            } },
            when {
                model.currentResponse?.data?.dynamic?.get(0)?.keys?.contains("blacklist") == true -> TIMEOUT_BLACKLIST
                JWTUtils.isDisplayPremium() -> TIMEOUT_PREMIUM
                else -> TIMEOUT
            }
        )
    }

    override fun onResume() {
        Log.d(TAG, "on Resume")
        super.onResume()
        if (presenter == null) {
            presenter = context?.let { ResultScanPresenterImpl(this) }
        }
        if(!BuildConfig.WITH_CHECK_HELP) {
            activity?.window?.setFlags(
                WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE
            )
        }
        context?.let { c ->
            SynchronisationUtils.checkStep(c, true)
        }
    }

    override fun onStop() {
        super.onStop()
        handler.removeMessages(0)
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_result, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun showNavigation(): MainActivity.NavigationIcon {
        return if(SynchronisationUtils.getCurrentStep() < 3) {
            MainActivity.NavigationIcon.BACK
        } else {
            MainActivity.NavigationIcon.NO_ICON
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_help -> {
                goToTutorialHelp()
                return true
            }
            R.id.action_home -> {
                goToActionChoice()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun goToActionChoice() {
        featureFragment?.popToTag(ActionChoiceChildFragment.TAG)
    }

    private fun goToScan() {
        featureFragment?.popToTag(ScanChildFragment.TAG)
    }

    private fun goToTutorialHelp() {
        if(JWTUtils.isDisplayPremium()) {
            featureFragment?.replaceFragment(TutorialResultPremiumHelpChildFragment.TAG)
        } else {
            featureFragment?.replaceFragment(TutorialResultLiteHelpChildFragment.TAG)
        }
    }

    override fun onItemClick(position: Int) {

        val data: ResultData = resultScan.datas[position]
        when (data.format) {
            Constants.CODE_VALIDITY_OK -> Utils.showViewToast(
                activity as Activity,
                R.layout.tooltip_check_validity_ok_view,
                R.id.cl_check_val_ok
            )
            Constants.CODE_VALIDITY_KO -> Utils.showViewToast(
                activity as Activity,
                R.layout.tooltip_check_validity_ko_view,
                R.id.cl_check_val_ko
            )
            Constants.CODE_CHECK_1 -> Utils.showViewToast(
                activity as Activity,
                R.layout.tooltip_check_1_view,
                R.id.cl_check1
            )
            Constants.CODE_CHECK_3 -> Utils.showViewToast(
                activity as Activity,
                R.layout.tooltip_check_3_view,
                R.id.cl_check3
            )
            Constants.CODE_ICONE_1 -> Utils.showViewToast(
                activity as Activity,
                R.layout.tooltip_icone_1_view,
                R.id.cl_icone_1
            )
        }
    }

    override fun showLoading(show: Boolean) {
        Log.d(TAG, "show loading $show")
        EventBus.getDefault().post(ShowLoadingAnimEvent(activity, show))
    }

    private fun vibrateIfInvalid() {
        if (resultScan.datas.size >= 1) {
            if (Constants.CODE_VALIDITY_KO == resultScan.datas[0].format) {

                when {
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
                        @SuppressLint("WrongConstant")
                        val vibratorManager = context?.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
                        vibratorManager.defaultVibrator.vibrate(
                            VibrationEffect.createOneShot(
                                500,
                                VibrationEffect.DEFAULT_AMPLITUDE
                            )
                        )
                    }
                    Build.VERSION.SDK_INT >= 26 -> {
                        @Suppress("DEPRECATION")
                        (context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(
                            VibrationEffect.createOneShot(
                                500,
                                VibrationEffect.DEFAULT_AMPLITUDE
                            )
                        )
                    }
                    else -> {
                        @Suppress("DEPRECATION")
                        (context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(500)
                    }
                }
            }
        }

    }
}