package com.ingroupe.verify.anticovid.ui.init

interface InitView {
    fun setupFinished()
}