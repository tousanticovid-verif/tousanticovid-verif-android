package com.ingroupe.verify.anticovid.auth

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.*


class AcceptLanguageHeaderInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest: Request = chain.request()
        val requestWithHeaders: Request = originalRequest.newBuilder()
            .header("Accept-Language", Locale.getDefault().language)
            .build()
        return chain.proceed(requestWithHeaders)
    }
}