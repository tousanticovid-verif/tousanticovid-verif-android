package com.ingroupe.verify.anticovid.ui.statistic

import com.ingroupe.verify.anticovid.common.model.StatsDay
import com.ingroupe.verify.anticovid.common.model.StatsPeriod

interface StatisticPresenter {

    fun formatMap(statPeriod : StatsPeriod): Map<Long, StatsDay>

}