package com.ingroupe.verify.anticovid

import android.app.Application
import android.content.Context
import com.blongho.country_data.World

class App : Application() {
    companion object {
        lateinit var instance: App private set

        fun getContext(): Context {
            return instance.applicationContext
        }
    }


    override fun onCreate() {
        super.onCreate()
        instance = this

        //Init of the Lib used to retrieve all the flags
        World.init(this)
    }
}