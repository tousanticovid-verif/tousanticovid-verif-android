/*
 *  ---license-start
 *  eu-digital-green-certificates / dgca-verifier-app-android
 *  ---
 *  Copyright (C) 2021 T-Systems International GmbH and all other contributors
 *  ---
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  ---license-end
 *
 *  Created by osarapulov on 6/25/21 9:28 AM
 */

package com.ingroupe.verify.anticovid.data.local

import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import dgca.verifier.app.engine.data.source.countries.CountriesRepository
import dgca.verifier.app.engine.data.source.local.countries.CountriesLocalDataSource
import kotlinx.coroutines.flow.Flow
import java.util.*

class TacvCountriesRepository(
    private val localDataSource: CountriesLocalDataSource
) : CountriesRepository {

    companion object {
        const val TAG = "CountriesRepository"
    }

    override suspend fun preLoadCountries(countriesUrl: String) {
        val listCountries = ConfServiceUtils.getCountries()
        if(listCountries.isNotEmpty()) {
            listCountries.map { it.lowercase(Locale.ROOT) }
                .apply { localDataSource.updateCountries(this) }
        } else {
            throw Exception("List of countries is empty")
        }
    }

    override fun getCountries(): Flow<List<String>> {
        return localDataSource.getCountries()
    }

    override suspend fun initDatas(): Boolean {
        val countries : List<String>? = Utils.loadFromAsset("sync/sync_country.json")

        if (countries.isNullOrEmpty()) {
            return false
        }

        localDataSource.updateCountries(countries)
        return true
    }
}
